package uk.ac.cam.cl.dtg.teaching.chime.dao;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static com.google.common.truth.Truth.assertThat;
import static com.google.common.truth.Truth8.assertThat;

import com.google.common.collect.ImmutableList;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.IntStream;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.ac.cam.cl.dtg.teaching.chime.database.InMemoryDatabase;

@RunWith(JUnit4.class)
public class LocalRepoDaoTest {

  private InMemoryDatabase database;

  @Before
  public void setup() {
    database = new InMemoryDatabase();
  }

  @Test
  public void insert_succeeds() {
    // ARRANGE
    UserDao userDao =
        database.execute(q -> UserDao.insert("TEST", UserDao.ROLE_ADMIN, "TEST FULL NAME", q));
    NewLocalRepo newLocalRepo =
        NewLocalRepo.builder()
            .potteryRepoId("POTTERY-REPO-ID")
            .repoName("REPO-NAME")
            .ownerName("OWNER-NAME")
            .potteryTaskId("POTTERY-TASK-ID")
            .user(userDao)
            .variant("VARIANT")
            .startTime(Instant.now())
            .build();

    // ACT
    LocalRepo localRepo = database.execute(newLocalRepo::insert);

    // ASSERT
    Optional<LocalRepo> found =
        database.execute(q -> LocalRepo.lookup("OWNER-NAME", "REPO-NAME", q));
    assertThat(found).hasValue(localRepo);
  }

  @Test
  public void lookupAll_findsAll() {
    // ARRANGE
    UserDao userDao =
        database.execute(q -> UserDao.insert("TEST", UserDao.ROLE_ADMIN, "TEST FULL NAME", q));
    ImmutableList<LocalRepo> localRepo =
        IntStream.range(0, 10)
            .mapToObj(
                i ->
                    NewLocalRepo.builder()
                        .potteryRepoId("POTTERY-REPO-ID-" + i)
                        .repoName("REPO-NAME-" + i)
                        .ownerName("OWNER-NAME-" + i)
                        .potteryTaskId("POTTERY-TASK-ID-" + i)
                        .user(userDao)
                        .variant("VARIANT")
                        .startTime(Instant.now())
                        .build())
            .map(n -> database.execute(n::insert))
            .collect(toImmutableList());

    // ACT
    ImmutableList<LocalRepo> found = database.execute(q -> LocalRepo.lookup(userDao, q));

    // ASSERT
    assertThat(found).containsExactlyElementsIn(localRepo);
  }

  @Test
  public void lookupAllByTaskId_findsAll() {
    // ARRANGE
    UserDao userDao =
        database.execute(q -> UserDao.insert("TEST", UserDao.ROLE_ADMIN, "TEST FULL NAME", q));
    ImmutableList<LocalRepo> localRepo =
        IntStream.range(0, 10)
            .mapToObj(
                i ->
                    NewLocalRepo.builder()
                        .potteryRepoId("POTTERY-REPO-ID-" + i)
                        .repoName("REPO-NAME-" + i)
                        .ownerName("OWNER-NAME-" + i)
                        .potteryTaskId("POTTERY-TASK-ID-" + i)
                        .user(userDao)
                        .variant("VARIANT")
                        .startTime(Instant.now())
                        .build())
            .map(n -> database.execute(n::insert))
            .collect(toImmutableList());

    // ACT
    ImmutableList<LocalRepo> found =
        database.execute(q -> LocalRepo.lookupAllByTask("POTTERY-TASK-ID-1", q));

    // ASSERT
    assertThat(found).containsExactly(localRepo.get(1));
  }
}
