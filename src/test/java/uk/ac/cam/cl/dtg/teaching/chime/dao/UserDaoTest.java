package uk.ac.cam.cl.dtg.teaching.chime.dao;

import static com.google.common.truth.Truth8.assertThat;

import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.ac.cam.cl.dtg.teaching.chime.database.InMemoryDatabase;

@RunWith(JUnit4.class)
public class UserDaoTest {

  private InMemoryDatabase database;

  @Before
  public void setup() {
    database = new InMemoryDatabase();
  }

  @Test
  public void insert_succeeds() {
    // ARRANGE

    // ACT
    UserDao userDao =
        database.execute(q -> UserDao.insert("TEST", UserDao.ROLE_ADMIN, "ADMIN FULL NAME", q));

    // ASSERT
    Optional<UserDao> found = database.execute(q -> UserDao.lookup("TEST", q));
    assertThat(found).hasValue(userDao);
  }
}
