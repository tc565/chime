package uk.ac.cam.cl.dtg.teaching.chime.app;

import static com.google.common.truth.Truth.assertThat;

import org.junit.Test;

public class FormatterTest {

  @Test
  public void format_doesNotCrash_withJava() {
    // ARRANGE
    String code = "public class C {}";
    String filename = "C.java";
    Formatter formatter = new Formatter("1");

    // ACT
    String result = formatter.format(code, filename);

    // ASSERT
    assertThat(result).isNotEmpty();
  }
}
