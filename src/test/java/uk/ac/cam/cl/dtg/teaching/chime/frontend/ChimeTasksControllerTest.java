package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import static org.junit.Assert.fail;

import de.jkeylockmanager.manager.KeyLockManager;
import de.jkeylockmanager.manager.KeyLockManagers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateConfiguration;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.InMemoryDatabase;
import uk.ac.cam.cl.dtg.teaching.pottery.api.mock.PotteryBackendMock;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.RepoStorageException;

@RunWith(JUnit4.class)
public class ChimeTasksControllerTest {
  private ChimeTasksController tasksController;
  private InMemoryDatabase database;

  @Before
  public void setup() {
    database = new InMemoryDatabase();
    Messages messages = new Messages();
    HttpConnectionUri httpConnectionUri = new HttpConnectionUri("http://test.com/", "/api/");
    GitLocation gitLocation = new GitLocation("test.com", 222, "/git/root/dir");
    TemplateProcessor templateProcessor =
        new TemplateProcessor(
            new TemplateConfiguration(),
            "http://test.com/",
            false,
            "",
            messages,
            httpConnectionUri,
            gitLocation);
    PotteryInterface potteryInterface = new PotteryInterface(new PotteryBackendMock(), false);
    KeyLockManager keyLockManager = KeyLockManagers.newLock();
    tasksController =
        new ChimeTasksController(
            templateProcessor,
            potteryInterface,
            database,
            keyLockManager,
            httpConnectionUri,
            gitLocation,
            messages);
  }

  @Test
  public void start_throws_withInvalidRepoName() {
    // ARRANGE
    UserDao requestingUser =
        database.execute(q -> UserDao.insert("TEST", UserDao.ROLE_ADMIN, "ADMIN FULL NAME", q));
    String invalidRepoName = "name/with/slash";

    // ACT
    try {
      tasksController.start(requestingUser, "ANY", invalidRepoName);
    } catch (RepoStorageException e) {
      return;
    }
    fail("Failed to throw RepoStorageException");
  }
}
