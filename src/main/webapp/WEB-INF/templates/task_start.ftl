<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="${task.name()}"
current = "none"
owningUser=authenticatedUser.userName()
>
<div class="jumbotron">
    <h1 class="display-4">${task.name()}</h1>
    <p class="lead">${task.problemStatement()?no_esc}</p>
    <form action="${task.taskUrls().startTask()}" method="POST">
        <div class="form-group">
            <label for="nameId">Repository name</label>
            <input type="text" class="form-control" id="nameId" name="name" value="${defaultName}" rows="3"/>
        </div>
        <input type="submit"
               class="btn btn-primary btn-lg" value="Start task"/>
    </form>
</div>
</@c.page>
