<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="Permissions"
current = "permissions"
owningUser=authenticatedUser.userName()
>
    <div class="jumbotron">
        <h1 class="display-4">Permissions</h1>
        <p class="lead">No user can see the tasks undertaken by another unless explicit access is given. You may
            delegate access to those users which you can view to any other user.</p>
    </div>
    <div class="card mb-3">
        <div class="card-header">Grant permission</div>
        <div class="card-body">
            <form action="${urlPrefix}/page/permissions/grant" method="POST">
                <div class="form-group row">
                    <label for="username" class="col-sm-3 col-form-label">Viewing user</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="viewingusername" name="viewer"
                               aria-describedby="usernameHelp"
                               placeholder="Enter CRSID" required>
                        <small id="usernameHelp" class="form-text text-muted">CRSID of user who should be granted
                            permission.</small>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="exampleInputPassword1" class="col-sm-3 col-form-label">Viewed users</label>
                    <div class="col-sm-9">
                        <#list viewable as viewed>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="checkbox${viewed.userName()}" name="grant"
                                       value="${viewed.userName()}">
                                <label class="form-check-label" for="checkbox${viewed.userName()}">
                                    <#if viewed.exists()>
                                        <a href="${commonUrls.usersTasks()}${viewed.userName()}">${viewed.userName()}</a>
                                    <#else >
                                        ${viewed.userName()}
                                    </#if>
                                </label>
                            </div>
                        </#list>
                        <small id="usernameHelp" class="form-text text-muted">Users that should be made visible.</small>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Grant</button>
            </form>
        </div>
    </div>
    <div class="card mb-3">
        <div class="card-header">Existing permissions</div>
        <div class="card-body">
            <#list viewers.keySet() as viewer>
                <div class="d-flex mb-3">
                    <div class="col-sm-3">
                        <a href="${commonUrls.usersTasks()}/${viewer}">${viewer}</a>
                    </div>
                    <div class="col-sm-8">
                        <#list viewers.get(viewer) as viewed>
                            <#if viewed.exists()>
                                <a href="${commonUrls.usersTasks()}${viewed.userName()}">${viewed.userName()}</a>
                            <#else >
                                ${viewed.userName()}
                            </#if>
                        </#list>
                    </div>
                    <div class="col-sm-1">
                        <form action="${urlPrefix}/page/permissions/revoke" method="POST">
                            <input type="hidden" name="viewer" value="${viewer}"/>
                            <#list viewers.get(viewer) as viewed>
                                <input type="hidden" name="viewed" value="${viewed}"/>
                            </#list>
                            <button type="submit" class="btn btn-danger">Revoke</button>
                        </form>
                    </div>
                </div>
            </#list>
        </div>
    </div>
</@c.page>
