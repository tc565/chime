<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="Leaderboard"
current = "leaderboard"
owningUser=authenticatedUser.userName()
>

    <h1>Leaderboard</h1>

    <p>The table below shows the score from your most recent passing submission. Scores from the agents included with
        the Mario-AI-Framework are also shown for reference.</p>

    <table class="table">
        <thead>
        <tr>
            <th scope="col">#</th>
            <th scope="col">CRSID</th>
            <th scope="col">Score</th>
            <th scope="col">Submission time</th>
        </tr>
        </thead>
        <tbody>
        <#list results as result>
            <#if result.reference()>
                <tr style="color: silver">
                    <th scope="row">&nbsp;</th>
                    <td>${result.username()}</td>
                    <td>${result.score()}</td>
                    <td>&nbsp;</td>
                </tr>
            <#else>
                <tr>
                    <th scope="row">${result.rank()}</th>
                    <td>${result.username()}</td>
                    <td>${result.score()}</td>
                    <td>${result.timeRecorded()?string["yyyy-MM-dd HH:mm"]}</td>
                </tr>
            </#if>
        </#list>
        </tbody>
    </table>

</@c.page>
