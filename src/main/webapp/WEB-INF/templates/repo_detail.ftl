<#ftl output_format="HTML">
<#import "common.ftl" as c/>

<@c.page
title=repo.taskName()
current = "summary"
owningUser=repo.localRepo().ownerName()
>

    <#if repo.tick().isPresent() && authenticatedUser.isAdmin()>
        <div class="row">
            <div class="col-sm-6">
                <@cardDeadline/>
            </div>
            <div class="col-sm-6">
                <@cardTick/>
            </div>
        </div>
    </#if>

    <div class="jumbotron">
        <p class="h1 mr-auto">${repo.taskName()}</p>
        <p class="lead">${repo.problemStatement()?no_esc}</p>

        <#if leaderboard>
            <p>
                <i class="fas fa-trophy"></i> This task has a leaderboard!: <a href="${repo.repoUrls().leaderboard()}">View
                    leaderboard</a>
            </p>
        </#if>
    </div>

    <#if repo.tick().isPresent()>
        <div class="row">
            <div class="col-sm-6">
                <@cardClone/>
            </div>
            <div class="col-sm-6">
                <@cardAssessed/>
            </div>
        </div>
    <#else>
        <@cardClone/>
    </#if>

    <@cardCommit/>

    <#list repo.actions() as action>
        <@cardSubmission action=action index=action?counter/>
    </#list>

    <#if
    (!repo.tick().isPresent() || repo.tick().get().tickState() == "NOT_ASSESSED") &&
    (authenticatedUser.isAdmin() || authenticatedUser.userId() == repo.localRepo().user().userId())
    >
        <@cardDelete/>
    </#if>

    <#if authenticatedUser.isAdmin()>
        <p><small>Task Id: ${repo.localRepo().potteryTaskId()}</small></p>
    </#if>

</@c.page>


<#macro cardTick>
    <div class="card mb-2">
        <div class="card-body">
            <form action="${repo.repoUrls().tickState()}" method="POST">
                <input type="hidden" name="sha1"
                       value="${repo.lastAction().latestSubmission().isPresent()?then(repo.lastAction().latestSubmission().get().submission().sha1(),'N/A')}"/>
                <div class="d-flex">
                    <div>
                        <div>
                            <#switch repo.tick().get().tickState()>
                                <#case "NOT_ASSESSED">
                                    <i class="fas fa-bed fa-fw"></i> Not assessed
                                    <#break>
                                <#case "NO_SHOW">
                                    <i class="far fa-frown fa-fw"></i> Failed to attend
                                    <#break>
                                <#case "CORRECTION_NEEDED">
                                    <i class="fas fa-bug fa-fw"></i> Correction needed
                                    <#break>
                                <#case "AWARDED">
                                    <i class="fas fa-check fa-fw"></i> Passed tick
                            </#switch>
                        </div>
                        <#if repo.tick().get().assessedBy() != 'N/A'>
                            <div>Ticked by: ${repo.tick().get().assessedBy()}</div>
                            <div>Version:
                                <#if repo.tick().get().sha1()?length gt 8>
                                    ${repo.tick().get().sha1()[0..8]}
                                <#else>
                                    ${repo.tick().get().sha1()}
                                </#if>
                            </div>
                        </#if>
                    </div>
                    <div class="ml-auto">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="tickState" id="inlineRadio1"
                                   value="NOT_ASSESSED"
                                    ${(repo.tick().get().tickState() == "NOT_ASSESSED")?then("checked","")}>
                            <label class="form-check-label" for="inlineRadio1">Not assessed</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="tickState" id="inlineRadio2"
                                   value="NO_SHOW"
                                    ${(repo.tick().get().tickState() == "NO_SHOW")?then("checked","")}>
                            <label class="form-check-label" for="inlineRadio2">Failed to attend</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="tickState" id="inlineRadio3"
                                   value="CORRECTION_NEEDED"
                                    ${(repo.tick().get().tickState() == "CORRECTION_NEEDED")?then("checked","")}>
                            <label class="form-check-label" for="inlineRadio3">Correction needed</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="tickState" id="inlineRadio4"
                                   value="AWARDED"
                                    ${(repo.tick().get().tickState() == "AWARDED")?then("checked","")}>
                            <label class="form-check-label" for="inlineRadio4">Passed tick</label>
                        </div>
                    </div>
                    <div>
                        <input type="submit" class="btn btn-primary" value="Update"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</#macro>

<#macro cardClone>
    <div class="card mb-2">
        <div class="card-body">
            <p>
                <span class="fas fa-code-branch mr-2"></span>
                Clone with SSH</p>
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">git clone</span>
                </div>
                <input class="form-control" readonly="true" type="text"
                       value="${repo.repoUrls().gitClone()}"/>
            </div>
        </div>
    </div>
</#macro>

<#macro cardAssessed>
    <div class="card mb-2">
        <div class="card-body">
            <p>
                <#switch repo.tick().get().tickState()>
                    <#case "NOT_ASSESSED">
                        <i class="fas fa-bed fa-fw mr-2"></i>
                        <#break>
                    <#case "NO_SHOW">
                        <i class="fas fa-frown fa-fw mr-2"></i>
                        <#break>
                    <#case "CORRECTION_NEEDED">
                        <i class="fas fa-bug fa-fw mr-2"></i>
                        <#break>
                    <#case "AWARDED">
                        <i class="fas fa-check fa-fw mr-2"></i>
                        <#break>
                </#switch>
                Assessed Task -
                <#switch repo.tick().get().tickState()>
                    <#case "NOT_ASSESSED">
                        Not ticked yet
                        <#break>
                    <#case "NO_SHOW">
                        Failed to attend session
                        <#break>
                    <#case "CORRECTION_NEEDED">
                        Ticker requested corrections
                        <#break>
                    <#case "AWARDED">
                        Tick awarded
                        <#break>
                    <#default>
                        Status unknown
                </#switch>
                <#if repo.tick().get().online()>
                    <a href="${repo.tick().get().meetingUrl(meetingGenerationNonce)}">(Online ticking meeting)</a>
                </#if>
            </p>
            <p>A passing submission must be made before ${repo.tick().get().deadline()?string["yyyy-MM-dd HH:mm"]}</p>


        </div>
    </div>
</#macro>

<#macro cardSubmission action index>
    <div class="card mb-2">
        <div class="card-body">
            <#if action.active()>
                <@actionStatus latest=action.latestSubmission() index=index action=action/>
            <#else>
                <@actionStatus latest=action.latestPassed() index=index action=action/>
            </#if>
            <div class="d-flex">
                <div class="mr-auto collapse ${action.active()?then('show','')}"
                     id="collapse${action.name()}">
                    ${action.description()?no_esc}
                </div>
            </div>
            <#if action.active()>
                <#if authenticatedUser.isAdmin() || authenticatedUser.userId() == repo.localRepo().user().userId()>
                    <div class="d-flex">
                        <#if repo.tick().isPresent() && repo.tick().get().deadlinePassed()>
                            <div><p>The deadline for completing this exercise has now passed and no more submissions are
                                    permitted.</p>
                            </div>
                        <#else>
                            <#if action.latestSubmission().isPresent() && repo.latestCommit().isPresent() && action.latestSubmission().get().submission().sha1() == repo.latestCommit().get().sha1()>
                                <div><p>The latest version in your repository has already been tested.
                                        You should make a new commit if you wish to make a new submission for
                                        testing.</p>
                                </div>
                            <#else>
                                <div>Make a new submission to test the latest version of your repository.</div>
                                <div class="ml-auto">
                                    <form action="${repoUrls.newSubmission()}" method="POST">
                                        <input type="submit" class="btn btn-success"
                                               value="New submission"/>
                                    </form>
                                </div>
                            </#if>
                        </#if>
                    </div>
                </#if>
            </#if>
        </div>
    </div>
</#macro>

<#macro actionStatus latest index action>
    <div class="d-flex">
        <div class="mr-2">
            <span class="fas fa-puzzle-piece fa-fw"></span>
            <button class="btn btn-link ${action.active()?then('','collapsed')}"
                    data-toggle="collapse"
                    data-target="#collapse${action.name()}"
                    aria-expanded="${action.active()?c}"
                    aria-controls="collapse${action.name()}">
                Part ${index}:
                <#if latest.isPresent()>
                    ${latest.get().submission().status()} at
                    ${latest.get().submission().submissionTime()?string["yyyy-MM-dd HH:mm"]}
                <#else>
                    no submissions yet
                </#if>
            </button>
        </div>
        <div class="ml-auto">
            <#if latest.isPresent()>
                <a class="btn btn-primary ml-1 mb-1"
                   href="${latest.get().submissionUrls().report()}"
                   role="button">
                    <span class="fas fa-cogs fa-fw"></span> </a>
                <a class="btn btn-primary ml-1 mb-1"
                   href="${latest.get().submissionUrls().code()}"
                   role="button">
                    <span class="fas fa-code fa-fw"></span> </a>
            </#if>
        </div>
    </div>
</#macro>

<#macro cardDeadline>
    <div class="card mb-2">
        <div class="card-body">
            <div class="d-flex">
                <div class="mr-2">
                    <#if repo.lastAction().latestPassed().isPresent()>
                        Submission passed at
                        ${repo.lastAction().latestPassed().get().submission().submissionTime()?string["yyyy-MM-dd HH:mm"]}
                    <#else>
                        No submissions passed yet
                    </#if>
                </div>
                <div class="ml-auto">
                    <#if repo.lastAction().latestPassed().isPresent()>
                        <a class="btn btn-primary ml-1 mb-1"
                           href="${repo.lastAction().latestPassed().get().submissionUrls().report()}"
                           role="button">
                            <span class="fas fa-cogs fa-fw"></span> </a>
                        <a class="btn btn-primary ml-1 mb-1"
                           href="${repo.lastAction().latestPassed().get().submissionUrls().code()}"
                           role="button">
                            <span class="fas fa-code fa-fw"></span> </a>
                    </#if>
                </div>
            </div>
            <div class="d-flex">
                <div>Testing deadline: ${repo.tick().get().deadline()?string["yyyy-MM-dd HH:mm"]}</div>
                <div class="ml-auto">
                    <form action="${repo.repoUrls().extension()}" method="POST">
                        <input type="submit" class="btn btn-primary" value="+1 hr"/>
                    </form>
                </div>
            </div>
        </div>
    </div>
</#macro>

<#macro cardCommit>
    <div class="card mb-2">
        <div class="card-body">
            <div class="d-flex">
                <div>
                    Latest commit:
                    <#if repo.latestCommit().isPresent()>
                        ${repo.latestCommit().get().message()} at
                        ${repo.latestCommit().get().time()?string["yyyy-MM-dd HH:mm"]}
                    <#else>None
                    </#if>
                </div>
                <div class="ml-auto">
                    <a class="btn btn-primary ml-1 mb-1"
                       href="${repoUrls.exportSummary()}"
                       role="button"><span class="fas fa-file-export"></span></a>
                    <a class="btn btn-primary ml-1 mb-1"
                       href="${repoUrls.timeline()}"
                       role="button"><span class="fas fa-history fa-fw"></span></a>
                    <#if repo.latestCommit().isPresent()>
                        <a class="btn btn-primary ml-1 mb-1"
                           href="${repo.latestCommit().get().codeLink()}"
                           role="button"><span class="fas fa-code fa-fw"></span></a>
                    </#if>
                </div>
            </div>
        </div>
    </div>
</#macro>

<#macro cardDelete>
    <div class="card mb-2">
        <div class="card-body">
            <div class="d-flex">
                <div>
                    Delete repository: this will irreversibly delete all code and associated submissions
                </div>
                <div class="ml-auto">
                    <a class="btn btn-danger"
                       href="${repo.repoUrls().delete()}"
                       role="button"><span class="fas fa-trash fa-fw"></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</#macro>