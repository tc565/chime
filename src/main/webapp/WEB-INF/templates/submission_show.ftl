<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="${taskName} / Submission"
current = "none"
owningUser=submission.submission().localRepo().ownerName()
>
    <div class="card mb-2">
        <div class="card-body">
            <div class="d-flex">
                <div>
                    Submission
                    at ${submission.submission().submissionTime()?string["yyyy-MM-dd HH:mm"]}.
                    Status ${submission.submission().status()}.
                </div>
                <div class="ml-auto">
                    <a class="btn btn-primary"
                       href="${submission.submissionUrls().code()}" role="button"><i
                                class="fa fa-code"></i> ${submission.submission().sha1()[0..8]}</a>
                    <a class="btn btn-primary"
                       href="${submission.submissionUrls().repoUrls().summary()}"><i
                                class="fa fa-info-circle"></i> ${submission.submissionUrls().repoUrls().fullName()}</a>
                </div>
            </div>
            <#if submission.submission().status() == "PENDING">
                <div>
                    There are currently ${aheadInQueue} submissions ahead of you in the testing queue.
                </div>
            </#if>
        </div>
    </div>

    <#if submission.submission().errorMessage() != ''>
        <div class="alert alert-danger" role="alert">
            <i class="fas fa-exclamation-triangle"></i> ${submission.submission().errorMessage()}
        </div>
    </#if>

    <!-- Pottery repo id: ${submission.submission().localRepo().potteryRepoId()} -->

    <#list submission.submission().steps() as step>
        <div class="card mb-2">
            <div class="card-header">
                ${step.name()}
                <#switch step.status()>
                    <#case "COMPLETE">
                        (Completed in ${step.runtime().getSeconds()} seconds)
                        <#break>
                    <#case "FAILED">
                        (Failed in ${step.runtime().getSeconds()} seconds)
                        <#break>
                    <#default>
                        (Running)
                </#switch>
            </div>
            <div class="card-body">
                <#if step.output() == "">
                    <pre>&lt;no output&gt;</pre>
                <#else/>
                    <pre>${step.output()}</pre>
                </#if>
            </div>
        </div>
    </#list>

</@c.page>
