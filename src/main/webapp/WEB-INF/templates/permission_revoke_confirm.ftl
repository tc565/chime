<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="Revoke permission"
current = "none"
owningUser=authenticatedUser.userName()
>
    <form action="${urlPrefix}/page/permissions/revoke_confirm" method="POST">
        <input type="hidden" name="viewer" value="${viewer}"/>
        <#list viewed as v>
            <input type="hidden" name="viewed" value="${v}"/>
        </#list>
        <input type="submit" class="btn btn-danger" value="Revoke permission for ${viewer}"/>
    </form>
</@c.page>
