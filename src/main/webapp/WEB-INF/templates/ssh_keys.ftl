<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="SSH Keys"
current = "ssh_keys"
owningUser=authenticatedUser.userName()
>
    <div class="jumbotron">
        <h1 class="display-4">SSH Keys</h1>
        <p class="lead">We use SSH to authenticate access to your git repositories. Register the public part of your SSH
            keys here. Keep the private part a secret!</p>
        <p>Chime only supports the RSA type of SSH key. We recommend the Atlassian guide to
            <a href="https://www.atlassian.com/git/tutorials/install-git">installing git</a> and setting up
            <a href="https://www.atlassian.com/git/tutorials/git-ssh">git with SSH</a>. Alternatively, a better guide
            for Windows seems to be
            <a href="https://phoenixnap.com/kb/generate-ssh-key-windows-10">How To Generate SSH Key In Windows 10.</a>
            Note that on some systems (notably Arch Linux, and presumably others),
            <pre>PubkeyAcceptedKeyTypes +ssh-rsa</pre> must be set in <pre>~/.ssh/config</pre> or
            <pre>/etc/ssh/ssh_config</pre></p>
        <a class="btn btn-primary btn-lg" href="${urlPrefix}/page/ssh_keys/new" role="button">Register new
            key</a>
    </div>
    <#list keys as sshkey>
        <div class="card">
            <div class="card-body">
                <p class="card-text">${sshkey.key()}</p>
                <form action="${urlPrefix}/page/ssh_keys/delete" method="POST">
                    <input type="hidden" name="keyId" value="${sshkey.keyId()?c}"/>
                    <input class="btn btn-danger" type="submit" value="Delete">
                </form>
            </div>
        </div>
    </#list>
</@c.page>
