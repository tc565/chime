<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="${repo.taskName()} / Delete repository"
current = "none"
owningUser=repo.localRepo().ownerName()
>
    <form action="${repoUrls.delete()}" method="POST">
        <input type="submit" class="btn btn-danger"
               value="Delete ${repo.localRepo().ownerName()}/${repo.localRepo().repoName()}"/>
    </form>
</@c.page>
