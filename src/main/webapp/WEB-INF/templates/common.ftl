<#function active actual expected>
    <#if actual == expected>
        <#return "active"/>
    </#if>
    <#return ""/>
</#function>

<#macro head title owningUser>
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css"
              integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2"
              crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"
              integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ"
              crossorigin="anonymous">

        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.css"
              integrity="sha384-BdGj8xC2eZkQaxoQ8nSLefg4AV4/AwB3Fj+8SUSo7pnKP6Eoy18liIKTPn9oBYNG"
              crossorigin="anonymous">

        <link href="${urlPrefix}/details.css" rel="stylesheet">
        <link type="text/css" rel="stylesheet" href="${commonUrls.usersTasks()}highlight.css">
        <title>${title?html} ${(authenticatedUser.userName() != owningUser)?then('('+owningUser+')','')}</title>
    </head>
</#macro>

<#macro nav current>
    <nav class="navbar navbar-expand-md navbar-dark ${testingMode?then("bg-danger","bg-dark")} mb-4 sticky-top">
        <a class="navbar-brand" href="#">Chime</a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item ${active(current,'my_tasks')}">
                    <a class="nav-link" href="${commonUrls.usersTasks()}">
                        <span class="fas fa-book-open"></span> Tasks
                    </a>
                </li>
                <#if authenticatedUser.isAdmin()>
                    <li class="nav-item ${active(current,'all_tasks')}">
                        <a class="nav-link" href="${commonUrls.allTasks()}">
                            <span class="fas fa-globe"></span> All tasks
                        </a>
                    </li>
                </#if>
                <#if authenticatedUser.isAdmin() || authenticatedUser.isDelegator() || authenticatedUser.isViewer()>
                    <li class="nav-item ${active(current,'progress')}">
                        <a class="nav-link" href="${commonUrls.progress()}">
                            <span class="fas fa-binoculars"></span> Progress
                        </a>
                    </li>
                </#if>
                <#if authenticatedUser.isAdmin() || authenticatedUser.isDelegator()>
                    <li class="nav-item ${active(current,'permissions')}">
                        <a class="nav-link" href="${commonUrls.permissions()}">
                            <span class="fas fa-magic"></span> Permissions
                        </a>
                    </li>
                </#if>
                <li class="nav-item ${active(current,'ssh_keys')}">
                    <a class="nav-link" href="${commonUrls.sshKeys()}">
                        <span class="fas fa-key"></span> SSH keys
                    </a>
                </li>
                <li class="nav-item ${active(current,'research')}">
                    <a class="nav-link" href="${commonUrls.research()}">
                        <span class="fas fa-database"></span> Data consent
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav">
                <li class="nav-item" style="color:#ffffff">
                    ${authenticatedUser.userName()}
                    <#if authenticatedUser.isAdmin()>
                        <span class="fas fa-user-graduate"></span>
                    </#if>
                    <#if authenticatedUser.isViewer()>
                        <span class="fas fa-eye"></span>
                    </#if>
                </li>
            </ul>
        </div>
    </nav>
</#macro>

<#macro main>
    <main role="main" class="container-fluid">
        <#if messages != ''>
            <div class="alert alert-primary" role="alert">
                ${messages}
            </div>
        </#if>
        <#nested/>
    </main>
</#macro>

<#macro footer>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
            integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
            crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/katex@0.11.0/dist/katex.min.js"
            integrity="sha384-JiKN5O8x9Hhs/UE5cT5AAJqieYlOZbGT3CHws/y97o3ty4R7/O5poG9F3JoiOYw1"
            crossorigin="anonymous"></script>
    <script>
        var math = document.getElementsByClassName('language-math');
        for (var i = 0; i < math.length; i++) {
            katex.render(math[i].innerText, math[i]);
        }
    </script>
</#macro>

<#macro page title current owningUser>
    <!DOCTYPE html>
    <html lang="en">
    <@head title=title owningUser=owningUser/>
    <body>

    <@nav current=current/>

    <@main>
        <#nested/>
    </@main>

    <@footer/>

    </body>
    </html>
</#macro>

<#macro barepage title current>
    <!DOCTYPE html>
    <html lang="en">
    <@head title=title owningUser=authenticatedUser.userName()/>
    <body>

    <@main>
        <#nested/>
    </@main>

    <@footer/>

    </body>
    </html>
</#macro>

<#macro dashboard title current>
    <html lang="en">
    <@head title=title/>
    <body>

    <@nav current=current/>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 d-none d-md-block bg-light sidebar">
                <div class="sidebar-sticky">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link ${active(current,'summary')}"
                               href="${repoUrls.summary()}">
                                <span class="fas fa-project-diagram fa-fw mr-2"></span>
                                Summary
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ${active(current,'timeline')}"
                               href="${repoUrls.timeline()}">
                                <span class="fas fa-history fa-fw mr-2"></span>
                                Timeline
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link ${active(current,'code')}"
                               href="${repoUrls.code()}">
                                <span class="fas fa-code fa-fw mr-2"></span>
                                Code
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
            <@main>
                <#nested/>
            </@main>
        </div>

        <@footer/>
    </div>
    </body>
    </html>
</#macro>