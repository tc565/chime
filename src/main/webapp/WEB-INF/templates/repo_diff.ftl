<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="${repo.taskName()} / Diff"
current = "none"
owningUser=repo.localRepo().ownerName()
>

    <#list diffs.files() as diff>
        <div class="diff">
            <div class="side">
                <div class="diffheader">
                    ${diff.oldPath()}
                </div>
                <#list diff.lines() as line>
                    <#if line.operation() == "INSERT">
                        <div class="context"></div>
                    <#elseif line.operation() == "DELETE">
                        <div class="delete">${line.original()?no_esc}</div>
                    <#elseif line.operation() == "MODIFY" && line.original() != "">
                        <div class="delete">${line.original()?no_esc}</div>
                    <#elseif line.operation() == "OMITTED">
                        <div class="space">&hellip;</div>
                    <#else>
                        <div class="context">${line.original()?no_esc}</div>
                    </#if>

                </#list>
            </div>
            <div class="side">
                <div class="diffheader">
                    ${diff.newPath()}
                </div>
                <#list diff.lines() as line>
                    <#if line.operation() == "INSERT">
                        <div class="insert">${line.replacement()?no_esc}</div>
                    <#elseif line.operation() == "DELETE">
                        <div class="context"></div>
                    <#elseif line.operation() == "MODIFY">
                        <div class="insert">${line.replacement()?no_esc}</div>
                    <#elseif line.operation() == "OMITTED">
                        <div class="space">&hellip;</div>
                    <#else>
                        <div class="context">${line.replacement()?no_esc}</div>
                    </#if>
                </#list>
            </div>
        </div>
    </#list>

</@c.page>
