<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.barepage
title="${repo.localRepo().ownerName()}-${repo.localRepo().repoName()}"
current = "none"
>
    <span style="float:right">${repo.localRepo().ownerName()}</span>
    <h1>${repo.taskName()}</h1>
    ${repo.problemStatement()?no_esc}
    <#list repo.actions() as action>
        <@actionDetail action=action index=action?counter/>
    </#list>

</@c.barepage>

<#macro actionDetail action index>
    <h2>Part ${index}
        <span style="font-size: 50%">
        <#if action.activeSubmission().isPresent()>
            <#assign submission = action.activeSubmission().get().submission()>

            <#if submission.errorMessage() != ''>
                <span class="badge badge-danger"><i class="fas fa-exclamation-triangle"></i> Tests failed</span>
            <#else>
                <span class="badge badge-success"><i class="fas fa-check-circle"></i> Tests passed</span>
            </#if>
        <#else>
            <span class="badge badge-danger"><i class="fas fa-exclamation-triangle"></i>Not attempted</span>
        </#if>
        </span>
    </h2>
    <p>
        ${action.description()?no_esc}
    </p>
    <b>Solution</b>

    <#assign diff = action.getDiff(diffs)>
    <#if diff.files()?size == 0>
        <p><i>Not attempted yet</i></p>
    <#else>
        <#list diff.files() as file>
            <#if file.containsDeletion()>
                <@sideBySide file/>
            <#else>
                <@rightSide file/>
            </#if>
        </#list>
    </#if>

    <div class="clear"></div>
    <#if action.activeSubmission().isPresent()>
        <#assign submission = action.activeSubmission().get().submission()>
        <#if submission.errorMessage() != ''>
            <b class="mt-5">Test output</b>
            <#list submission.steps() as step>
                <#if step.output() != "">
                    <div class="diff">
                        <div class="side" style="white-space: pre">${step.output()}</div>
                    </div>
                </#if>
            </#list>
            <div class="clear"></div>
        </#if>
    </#if>
</#macro>

<#macro sideBySide file>
    <div class="diff">
        <div class="side">
            <div class="diffheader">
                ${file.oldPath()}
            </div>
            <#list file.lines() as line>
                <#if line.original() == "" && line.replacement() != "">
                    <div class="context"></div>
                <#elseif line.operation() == "INSERT">
                    <div class="context"></div>
                <#elseif line.operation() == "DELETE">
                    <div class="delete">${line.original()?no_esc}</div>
                <#elseif line.operation() == "MODIFY" && line.original() != line.replacement()>
                    <div class="delete">${line.original()?no_esc}</div>
                <#elseif line.operation() == "OMITTED">
                    <div class="space">&hellip;</div>
                <#else>
                    <div class="context">${line.original()?no_esc}</div>
                </#if>
            </#list>
        </div>
        <div class="side">
            <div class="diffheader">
                ${file.newPath()}
            </div>
            <#list file.lines() as line>
                <#if line.replacement() == "" && line.original() != "">
                    <div class="context"></div>
                <#elseif line.operation() == "INSERT">
                    <div class="insert">${line.replacement()?no_esc}</div>
                <#elseif line.operation() == "DELETE">
                    <div class="context"></div>
                <#elseif line.operation() == "MODIFY" && line.original() != line.replacement()>
                    <div class="insert">${line.replacement()?no_esc}</div>
                <#elseif line.operation() == "OMITTED">
                    <div class="space">&hellip;</div>
                <#else>
                    <div class="context">${line.replacement()?no_esc}</div>
                </#if>
            </#list>
        </div>
    </div>
</#macro>

<#macro rightSide file>
    <div class="diff">
        <div class="middle">
            <div class="diffheader">
                ${file.newPath()}
            </div>
            <#list file.lines() as line>
                <#if line.replacement() == "" && line.original() !="">
                    <div class="context"></div>
                <#elseif line.operation() == "INSERT">
                    <div class="insert">${line.replacement()?no_esc}</div>
                <#elseif line.operation() == "DELETE">
                    <div>${line.replacement()}</div>
                <#elseif line.operation() == "MODIFY">
                    <div class="insert">${line.replacement()?no_esc}</div>
                <#elseif line.operation() == "OMITTED">
                    <div class="space">&hellip;</div>
                <#else>
                    <div class="context">${line.replacement()?no_esc}</div>
                </#if>
            </#list>
        </div>
    </div>
</#macro>