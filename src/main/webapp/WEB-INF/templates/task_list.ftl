<#ftl output_format="HTML">
<#import "common.ftl" as c/>
<@c.page
title="All tasks"
current = "all_tasks"
owningUser=authenticatedUser.userName()
>
    <#list taskList as task>
        <div class="card">
            <div class="card-body">
                <p class="h5 card-title mr-auto">${task.name()}</p>
                <p class="card-text">${task.problemStatement()?no_esc}</p>
                <a href="${task.taskUrls().startTask()}" class="btn btn-primary">Start</a>
            </div>
        </div>
    </#list>
</@c.page>
