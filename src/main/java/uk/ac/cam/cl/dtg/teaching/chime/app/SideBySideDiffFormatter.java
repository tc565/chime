package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.github.difflib.text.DiffRow;
import com.github.difflib.text.DiffRowGenerator;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawText;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.patch.FileHeader;
import uk.ac.cam.cl.dtg.teaching.chime.dto.DiffDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.DiffFileDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.DiffLineDto;

public class SideBySideDiffFormatter extends DiffFormatter {

  private final DiffDto.Builder builder;
  private final int contextLines;
  private final DiffEntry entry;

  private SideBySideDiffFormatter(DiffDto.Builder builder, int contextLines, DiffEntry entry) {
    super(new ByteArrayOutputStream());
    this.builder = builder;
    this.contextLines = contextLines;
    this.entry = entry;
  }

  public static DiffDto format(
      List<DiffEntry> diff, String oldSha1, String newSha1, int contextLines, Repository repo) {
    DiffDto.Builder builder = DiffDto.builder().originalSha1(oldSha1).replacementSha1(newSha1);
    for (DiffEntry entry : diff) {
      try (DiffFormatter formatter = new SideBySideDiffFormatter(builder, contextLines, entry)) {
        formatter.setRepository(repo);
        formatter.format(entry);
      } catch (IOException e) {
        throw new RuntimeException("Impossible exception", e);
      }
    }
    return builder.build();
  }

  @Override
  public void format(FileHeader head, RawText a, RawText b) throws IOException {
    if (a == null
        || b == null
        || RawText.isBinary(a.getRawContent())
        || RawText.isBinary(b.getRawContent())) {
      return;
    }

    DiffFileDto.Builder diffFileBuilder =
        DiffFileDto.builder().oldPath(entry.getOldPath()).newPath(entry.getNewPath());

    DiffRowGenerator generator =
        DiffRowGenerator.create()
            .inlineDiffBySplitter(s -> new ArrayList<>(ImmutableList.of(s)))
            .build();

    List<DiffRow> rows = generator.generateDiffRows(lines(a), lines(b));
    ImmutableSet<Integer> indices = markLinesInScope(rows);
    int j = 0;
    boolean ommitted = false;
    for (DiffRow row : rows) {
      if (indices.contains(j)) {
        if (ommitted) {
          diffFileBuilder.addLine("", "", DiffLineDto.Operation.OMITTED);
        }
        diffFileBuilder.addLine(row.getOldLine(), row.getNewLine(), getOperation(row));
        ommitted = false;
      } else {
        ommitted = true;
      }
      j++;
    }
    builder.addFile(diffFileBuilder.build());
  }

  private static DiffLineDto.Operation getOperation(DiffRow row) {
    switch (row.getTag()) {
      case INSERT:
        return DiffLineDto.Operation.INSERT;
      case DELETE:
        return DiffLineDto.Operation.DELETE;
      case CHANGE:
        return DiffLineDto.Operation.MODIFY;
      case EQUAL:
        return DiffLineDto.Operation.CONTEXT;
    }
    throw new IllegalArgumentException("Unrecognised tag " + row.getTag());
  }

  private ImmutableSet<Integer> markLinesInScope(List<DiffRow> rows) {
    Set<Integer> indices = new HashSet<>();
    int i = 0;
    for (DiffRow row : rows) {
      if (!row.getOldLine().equals(row.getNewLine())) {
        for (int offset = -contextLines; offset < contextLines; offset++) {
          indices.add(i + offset);
        }
      }
      i++;
    }
    // If a line is skipped and the lines either side of it are included then we might as well
    // include this line since it will take that much space just to show it.
    for (int j = 0; j < i; j++) {
      if (!indices.contains(j) && indices.contains(j - 1) && indices.contains(j + 1)) {
        indices.add(j);
      }
    }
    return ImmutableSet.copyOf(indices);
  }

  private static ImmutableList<String> lines(RawText a) {
    int size = a.size();
    ImmutableList.Builder<String> builder = ImmutableList.builder();
    for (int i = 0; i < size; i++) {
      builder.add(a.getString(i));
    }
    return builder.build();
  }

  private static String formatLine(String line) {
    return line.replace(" ", "&nbsp;").replace("\t", "&nbsp;&nbsp;&nbsp;");
  }
}
