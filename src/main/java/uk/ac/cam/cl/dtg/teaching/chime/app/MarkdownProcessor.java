package uk.ac.cam.cl.dtg.teaching.chime.app;

import org.commonmark.node.AbstractVisitor;
import org.commonmark.node.Node;
import org.commonmark.node.Paragraph;
import org.commonmark.parser.Parser;
import org.commonmark.renderer.html.HtmlRenderer;
import org.commonmark.renderer.text.TextContentRenderer;

public class MarkdownProcessor {

  private static class FirstParagraphVisitor extends AbstractVisitor {
    private Paragraph firstParagraph = null;

    @Override
    public void visit(Paragraph paragraph) {
      if (firstParagraph == null) {
        firstParagraph = paragraph;
      }
      super.visit(paragraph);
    }
  }

  private static final int SHORT_TEXT_LENGTH = 255;

  public static String toHtml(String markdown) {
    Parser parser = Parser.builder().build();
    Node document = parser.parse(markdown);
    HtmlRenderer renderer = HtmlRenderer.builder().build();
    return renderer.render(document);
  }

  public static String toShortText(String markdown) {
    Parser parser = Parser.builder().build();
    Node document = parser.parse(markdown);
    FirstParagraphVisitor firstParagraphVisitor = new FirstParagraphVisitor();
    document.accept(firstParagraphVisitor);
    TextContentRenderer renderer = TextContentRenderer.builder().stripNewlines(true).build();
    String result =
        renderer.render(
            firstParagraphVisitor.firstParagraph != null
                ? firstParagraphVisitor.firstParagraph
                : document);
    if (result.length() > SHORT_TEXT_LENGTH) {
      result = result.substring(0, SHORT_TEXT_LENGTH);
      int lastSpace = result.lastIndexOf(' ');
      result = result.substring(0, lastSpace) + "...";
    }
    return result;
  }
}
