package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeTask;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.MarkdownProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskAction;
import uk.ac.cam.cl.dtg.teaching.chime.urls.TaskUrls;

@AutoValue
public abstract class TaskDto {

  public abstract String name();

  public abstract String potteryTaskId();

  public abstract String problemStatement();

  public abstract TaskUrls taskUrls();

  public abstract ImmutableList<TaskAction> actions();

  public static Builder builder() {
    return new AutoValue_TaskDto.Builder();
  }

  public static TaskDto fromTaskInfo(ChimeTask taskInfo, HttpConnectionUri httpConnectionUri) {

    return TaskDto.builder()
        .name(taskInfo.name())
        .problemStatement(MarkdownProcessor.toHtml(taskInfo.problemStatement()))
        .potteryTaskId(taskInfo.potteryTaskId())
        .taskUrls(TaskUrls.createNew(taskInfo.potteryTaskId(), httpConnectionUri))
        .actions(taskInfo.actions())
        .build();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder name(String name);

    public abstract Builder potteryTaskId(String potteryTaskId);

    public abstract Builder problemStatement(String problemStatement);

    public abstract Builder taskUrls(TaskUrls taskUrls);

    public abstract Builder actions(ImmutableList<TaskAction> actions);

    public abstract TaskDto build();
  }
}
