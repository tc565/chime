package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.urls.SubmissionUrls;

@AutoValue
public abstract class SubmissionDto {

  public abstract SubmissionDao submission();

  public abstract SubmissionUrls submissionUrls();

  public static Builder builder() {
    return new AutoValue_SubmissionDto.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {
    public abstract Builder submission(SubmissionDao submission);

    public abstract Builder submissionUrls(SubmissionUrls submissionUrls);

    public abstract SubmissionDto build();
  }
}
