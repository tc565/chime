package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@AutoValue
public abstract class ConfigDao {

  public abstract String key();

  public abstract String value();

  public static ConfigDao create(String key, String value) {
    return new AutoValue_ConfigDao(key, value);
  }

  public static Optional<ConfigDao> lookup(String key, TransactionQueryRunner q) {
    return q.query(
        "SELECT value from config where key=?",
        rs -> {
          if (!rs.next()) {
            return Optional.empty();
          }
          return Optional.of(create(key, rs.getString("value")));
        },
        key);
  }

  public void insert(TransactionQueryRunner q) {
    q.insert("insert into config(key,value) values (?,?)", key(), value());
  }
}
