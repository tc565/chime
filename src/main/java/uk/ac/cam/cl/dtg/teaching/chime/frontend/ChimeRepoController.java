package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static com.google.common.collect.ImmutableMap.toImmutableMap;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.ImmutableSortedSet;
import com.google.inject.Inject;
import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.api.errors.RefNotFoundException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.Constants;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.ObjectReader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.CanonicalTreeParser;
import org.eclipse.jgit.treewalk.TreeWalk;
import uk.ac.cam.cl.dtg.teaching.chime.app.AccessDeniedError;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeTask;
import uk.ac.cam.cl.dtg.teaching.chime.app.FileUtil;
import uk.ac.cam.cl.dtg.teaching.chime.app.Formatter;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitApiError;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryError;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.SideBySideDiffFormatter;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.Submissions;
import uk.ac.cam.cl.dtg.teaching.chime.dao.TickDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.chime.dto.DiffDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.FileDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.RepoActionDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.RepoDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.RepositoryEventDto;
import uk.ac.cam.cl.dtg.teaching.chime.dto.SubmissionDto;
import uk.ac.cam.cl.dtg.teaching.chime.kind.ReturnCodeWithLeaderboard;
import uk.ac.cam.cl.dtg.teaching.chime.kind.TaskKind;
import uk.ac.cam.cl.dtg.teaching.chime.urls.RepoUrls;
import uk.ac.cam.cl.dtg.teaching.chime.urls.SubmissionUrls;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

@Produces("text/html")
@Path("/repos")
public class ChimeRepoController {

  private final TemplateProcessor templateProcessor;
  private final Database database;
  private final PotteryInterface potteryInterface;
  private final HttpConnectionUri httpConnectionUri;
  private final GitLocation gitLocation;
  private final Messages messages;
  private final Formatter formatter;

  @Inject
  public ChimeRepoController(
      TemplateProcessor templateProcessor,
      Database database,
      PotteryInterface potteryInterface,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation,
      Messages messages,
      Formatter formatter) {
    this.templateProcessor = templateProcessor;
    this.database = database;
    this.potteryInterface = potteryInterface;
    this.httpConnectionUri = httpConnectionUri;
    this.gitLocation = gitLocation;
    this.messages = messages;
    this.formatter = formatter;
  }

  @GET
  @Path("/")
  public String list(@Context UserDao requestingUser) {
    return list(requestingUser, requestingUser.userName());
  }

  @GET
  @Path("/{owner}")
  public String list(@Context UserDao requestingUser, @PathParam("owner") String owner) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      UserDao ownerUser = UserDao.lookupOrThrow(owner, q);
      ownerUser.checkCanBeViewedBy(requestingUser, q);
      ImmutableList<RepoDto> repoList =
          LocalRepo.lookup(ownerUser, q).stream()
              .map(
                  repo -> {
                    try {
                      return Optional.of(createRepoDto(requestingUser, repo, q));
                    } catch (PotteryError e) {
                      return Optional.<RepoDto>empty();
                    }
                  })
              .filter(Optional::isPresent)
              .map(Optional::get)
              .sorted(
                  Comparator.comparing(RepoDto::taskName)
                      .thenComparing(r -> r.localRepo().repoName()))
              .collect(toImmutableList());
      return templateProcessor.process(
          "repo_list.ftl",
          requestingUser,
          ImmutableMap.of("repoList", repoList, "ownerName", owner));
    }
  }

  @GET
  @Path("/{owner}/{repoName}")
  public String info(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName)
      throws GitAPIException, IOException {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo repo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      ChimeTask taskInfo = potteryInterface.getTaskInfo(repo.potteryTaskId());
      boolean leaderboard = taskInfo.kind().equals(ReturnCodeWithLeaderboard.class.getSimpleName());

      RepoDto repoDto = createRepoDto(requestingUser, repo, q);

      ImmutableMap.Builder<String, Object> response = ImmutableMap.builder();
      response.put("repo", repoDto);
      response.put("leaderboard", leaderboard);
      try (Git g = Git.open(gitLocation.getDirectory(owner, repoName))) {
        RevCommit mostRecent = g.log().call().iterator().next();
        response.put(
            "latestCommit",
            RepositoryEventDto.createForCommit(mostRecent, repo, httpConnectionUri));
      } catch (NoHeadException e) {
        // ignore
      }

      return templateProcessor.process(
          "repo_detail.ftl", requestingUser, repoDto.repoUrls(), response.build());
    }
  }

  @GET
  @Path("/{owner}/{repoName}/delete")
  public String delete(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName) {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo repo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      repo.checkCanBeModifiedBy(requestingUser, q);
      RepoDto repoDto = createRepoDto(requestingUser, repo, q);
      return templateProcessor.process(
          "repo_delete_confirm.ftl",
          requestingUser,
          RepoUrls.createNew(requestingUser, repo, httpConnectionUri, gitLocation),
          ImmutableMap.of("repo", repoDto));
    }
  }

  @POST
  @Path("/{owner}/{repoName}/delete")
  public Response deleteConfirm(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName) {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo repo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      repo.checkCanBeModifiedBy(requestingUser, q);
      TickDao.TickState tickState =
          TickDao.lookup(repo, q).map(TickDao::tickState).orElse(TickDao.TickState.NOT_ASSESSED);

      if (!tickState.equals(TickDao.TickState.NOT_ASSESSED)) {
        messages.postMessage(
            requestingUser,
            "This repository has already been assessed by a ticker and so cannot be deleted.");
        return httpConnectionUri.redirectToFrontend(
            ChimeRepoController.class, repo.ownerName() + "/" + repo.repoName());
      }
      q.update("DELETE from repos where repoid=?", repo.repoId());
      FileUtil.deleteRecursive(gitLocation.getDirectory(owner, repoName));
      q.commit();
    } catch (IOException e) {
      throw new GitApiError("Failed to remove repository from disk.", e);
    }
    messages.postMessage(requestingUser, "Deleted repository " + owner + "/" + repoName);
    return httpConnectionUri.redirectToFrontend(ChimeRepoController.class, owner);
  }

  @POST
  @Path("/{owner}/{repoName}/submissions")
  public Response makeSubmission(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName) {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo repo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      repo.checkCanBeModifiedBy(requestingUser, q);
      ChimeTask taskInfo = potteryInterface.getTaskInfo(repo.potteryTaskId());

      String headSha = resolveHeadSha(owner, repoName);

      Optional<TickDao> tick = TickDao.lookup(repo, q);
      if (tick.isPresent() && tick.get().deadlinePassed()) {
        messages.postMessage(requestingUser, "The deadline for making submissions has now passed.");
        return httpConnectionUri.redirectToFrontend(
            ChimeRepoController.class, repo.ownerName() + "/" + repo.repoName());
      }

      Optional<Integer> existingSubmissionId =
          SubmissionDao.lookupSubmissionId(repo.repoId(), headSha, q);
      if (existingSubmissionId.isPresent()) {
        messages.postMessage(
            requestingUser,
            "The latest version of this repository has already been submitted. "
                + "If you want to make a "
                + "new submission then please make a change to your repository and try again.");
        return httpConnectionUri.redirectToFrontend(
            ChimeRepoController.class, repo.ownerName() + "/" + repo.repoName());
      }

      String currentAction = Submissions.lookup(repo, taskInfo.actions(), q).currentAction().name();
      Submission s = potteryInterface.scheduleTest(repo, currentAction, headSha);
      int newSubmissionId = q.nextVal("submissionIds");
      TaskKind.loadProcessor(taskInfo.kind())
          .processSubmission(newSubmissionId, repo, new Date(), s, q, potteryInterface)
          .insert(q);
      q.commit();

      return httpConnectionUri.redirectToFrontend(
          ChimeSubmissionsController.class, newSubmissionId);
    }
  }

  @GET
  @Path("/{owner}/{repoName}/timeline")
  public String timeline(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName)
      throws GitAPIException, IOException {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo localRepo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      RepoDto repoDto = createRepoDto(requestingUser, localRepo, q);
      ImmutableList<RepositoryEventDto> events = getRepositoryEvents(localRepo, q);

      return templateProcessor.process(
          "repo_timeline.ftl",
          requestingUser,
          RepoUrls.createNew(requestingUser, localRepo, httpConnectionUri, gitLocation),
          ImmutableMap.of("events", events, "repo", repoDto));
    }
  }

  @POST
  @Path("/{owner}/{repoName}/tickstate")
  public Response tickState(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName,
      @FormParam("tickState") String tickState,
      @FormParam("sha1") String sha1) {
    if (!requestingUser.isAdmin()) {
      throw new AccessDeniedError("Only administrators can do this");
    }
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo localRepo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      TickDao tick =
          TickDao.lookup(localRepo, q)
              .orElseThrow(() -> new DatabaseError("Tick state not applicable to this repo"));
      TickDao.TickState state = TickDao.TickState.valueOf(tickState);
      tick.setTickState(requestingUser, state, sha1, q);
      q.commit();
      messages.postMessage(requestingUser, "Recorded " + tickState + " for this repository");
    }
    return httpConnectionUri.redirectToFrontend(ChimeRepoController.class, owner + "/" + repoName);
  }

  @POST
  @Path("/{owner}/{repoName}/extension")
  public Response extension(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName) {
    if (!requestingUser.isAdmin()) {
      throw new AccessDeniedError("Only administrators can do this");
    }

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      LocalRepo localRepo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      TickDao tick =
          TickDao.lookup(localRepo, q)
              .orElseThrow(() -> new DatabaseError("Deadlines not applicable to this repo"));
      Date newDeadline =
          tick.deadline().before(new Date())
              ? Date.from(Instant.now().plus(Duration.ofHours(1)))
              : Date.from(tick.deadline().toInstant().plus(Duration.ofHours(1)));
      tick.setDeadline(newDeadline, q);
      messages.postMessage(requestingUser, "Updated deadline to " + newDeadline);
      q.commit();
    }
    return httpConnectionUri.redirectToFrontend(ChimeRepoController.class, owner + "/" + repoName);
  }

  @GET
  @Path("/highlight.css")
  @Produces("text/css")
  public String styleSheet() {
    return formatter.getStyle();
  }

  @GET
  @Path("/{owner}/{repoName}/code")
  public String code(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName)
      throws IOException {
    String headSha = resolveHeadSha(owner, repoName);
    return code(requestingUser, owner, repoName, headSha);
  }

  @GET
  @Path("/{owner}/{repoName}/code/{sha1}")
  public String code(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName,
      @PathParam("sha1") String sha1)
      throws IOException {

    LocalRepo localRepo;
    RepoDto repoDto;
    Optional<SubmissionDto> submission;
    boolean canBeModifiedByRequestingUser = false;
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      localRepo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      repoDto = createRepoDto(requestingUser, localRepo, q);
      submission =
          SubmissionDao.lookupSubmissionId(localRepo.repoId(), sha1, q)
              .flatMap(id -> SubmissionDao.lookup(id, q))
              .map(s -> createSubmissionDto(requestingUser, s, httpConnectionUri, gitLocation));
      canBeModifiedByRequestingUser = localRepo.user().canBeModifiedBy(requestingUser, q);
    }

    ImmutableSortedSet.Builder<FileDto> builder =
        ImmutableSortedSet.orderedBy(Comparator.comparing(FileDto::fileName));
    try (Git g = Git.open(gitLocation.getDirectory(owner, repoName))) {
      Repository repo = g.getRepository();
      try (RevWalk revWalk = new RevWalk(repo)) {
        RevTree tree = getRevTree(sha1, repo, revWalk);
        try (TreeWalk treeWalk = new TreeWalk(repo)) {
          treeWalk.addTree(tree);
          treeWalk.setRecursive(true);
          while (treeWalk.next()) {
            String filename = treeWalk.getPathString();
            if (hasAcceptedExtension(filename)) {
              ObjectId objectId = treeWalk.getObjectId(0);
              ObjectLoader loader = repo.open(objectId);
              String contents = formatter.format(new String(loader.getBytes()), filename);
              builder.add(FileDto.create(filename, contents, getBrush(filename)));
            }
          }
        }
      }
    }

    String headSha = resolveHeadSha(owner, repoName);

    return templateProcessor.process(
        "repo_code.ftl",
        requestingUser,
        RepoUrls.createNew(requestingUser, localRepo, httpConnectionUri, gitLocation),
        ImmutableMap.of(
            "files",
            builder.build(),
            "submission",
            submission,
            "head",
            headSha.equals(sha1),
            "repo",
            repoDto,
            "canModify",
            canBeModifiedByRequestingUser));
  }

  private static String getBrush(String fileName) {
    if (fileName.equals("Makefile")) {
      return "bash";
    }
    String extension = getExtension(fileName);
    switch (extension) {
      case "java":
      case "xml":
      case "pl":
        return extension;
      case "c":
      case "h":
      case "cpp":
        return "cpp";
    }
    return "text";
  }

  private static String getExtension(String fileName) {
    int lastDot = fileName.lastIndexOf(".");
    if (lastDot == -1) {
      return "";
    }
    return fileName.substring(lastDot + 1);
  }

  private static boolean hasAcceptedExtension(String fileName) {
    switch (fileName) {
      case "LICENSE":
      case "NOTICE":
      case "Makefile":
      case "link.ld":
        return true;
    }
    ImmutableSet<String> acceptedExtensions =
        ImmutableSet.of(
            "java", "xml", "md", "txt", "s", "c", "cpp", "v", "sv", "qsf", "qpf", "qsys", "bsv",
            "pl");

    return acceptedExtensions.contains(getExtension(fileName));
  }

  @GET
  @Path("/{owner}/{repoName}/diff/{oldSha1}/{newSha1}")
  public String diff(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName,
      @PathParam("oldSha1") String oldSha1,
      @PathParam("newSha1") String newSha1)
      throws GitAPIException, IOException {

    LocalRepo localRepo;
    RepoDto repoDto;
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      localRepo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      repoDto = createRepoDto(requestingUser, localRepo, q);
    }
    DiffDto diff = diffRepo(owner, repoName, oldSha1, newSha1);
    return templateProcessor.process(
        "repo_diff.ftl",
        requestingUser,
        RepoUrls.createNew(requestingUser, localRepo, httpConnectionUri, gitLocation),
        ImmutableMap.of("diffs", diff, "repo", repoDto));
  }

  @GET
  @Path("/{owner}/{repoName}/summary")
  public String summary(
      @Context UserDao requestingUser,
      @PathParam("owner") String owner,
      @PathParam("repoName") String repoName)
      throws GitAPIException, IOException {
    LocalRepo localRepo;
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      localRepo = LocalRepo.lookupOrThrow(requestingUser, owner, repoName, q);
      RepoDto repoDto = createRepoDto(requestingUser, localRepo, q);
      ImmutableList<RepositoryEventDto> events =
          getRepositoryEvents(localRepo, q).stream()
              .filter(e -> !e.submissionLink().isPresent())
              .collect(toImmutableList())
              .reverse();
      String baseSha = findBaseCommit(events);
      ImmutableList.Builder<DiffDto> diffs = ImmutableList.builder();
      for (RepoActionDto actionDto : repoDto.actions()) {
        if (actionDto.activeSubmission().isPresent()) {
          String newSha1 = actionDto.activeSubmission().get().submission().sha1();
          diffs.add(diffRepo(owner, repoName, baseSha, newSha1));
          Optional<RepositoryEventDto> following = following(newSha1, events);
          baseSha =
              following.isPresent()
                      && following
                          .get()
                          .message()
                          .equals(ChimeSubmissionsController.INITIALISED_NEXT_STEP)
                  ? following.get().sha1()
                  : newSha1;
        }
      }
      return templateProcessor.process(
          "repo_summary.ftl",
          requestingUser,
          RepoUrls.createNew(requestingUser, localRepo, httpConnectionUri, gitLocation),
          ImmutableMap.of("repo", repoDto, "diffs", diffs.build()));
    }
  }

  private Optional<RepositoryEventDto> following(
      String sha1, ImmutableList<RepositoryEventDto> events) {
    Iterator<RepositoryEventDto> step = events.iterator();
    while (step.hasNext()) {
      RepositoryEventDto next = step.next();
      if (next.sha1().equals(sha1)) {
        return step.hasNext() ? Optional.of(step.next()) : Optional.empty();
      }
    }
    return Optional.empty();
  }

  private String findBaseCommit(ImmutableList<RepositoryEventDto> events) {
    return events.stream()
        .filter(e -> e.message().equals(ChimeTasksController.INITIALISED_TASK))
        .findFirst()
        .map(RepositoryEventDto::sha1)
        .orElse(events.get(0).sha1());
  }

  private ImmutableList<RepositoryEventDto> getRepositoryEvents(
      LocalRepo localRepo, TransactionQueryRunner q) throws GitAPIException, IOException {
    ImmutableMap<String, RepositoryEventDto> submissions =
        SubmissionDao.lookup(localRepo, q).stream()
            .map(
                submission ->
                    RepositoryEventDto.createForSubmission(
                        submission, localRepo, httpConnectionUri))
            .collect(toImmutableMap(RepositoryEventDto::sha1, Function.identity()));
    ImmutableList.Builder<RepositoryEventDto> events = ImmutableList.builder();
    try (Git g = Git.open(gitLocation.getDirectory(localRepo.ownerName(), localRepo.repoName()))) {
      Iterator<RevCommit> currentCommitIterator = g.log().call().iterator();
      Iterator<RevCommit> previousCommitIterator = g.log().call().iterator();
      previousCommitIterator.next();

      while (currentCommitIterator.hasNext()) {
        RevCommit revForEvent = currentCommitIterator.next();
        String sha1 = revForEvent.toObjectId().getName();
        if (submissions.containsKey(sha1)) {
          events.add(submissions.get(sha1));
        }
        if (previousCommitIterator.hasNext()) {
          events.add(
              RepositoryEventDto.createForPatch(
                  revForEvent, previousCommitIterator.next(), localRepo, httpConnectionUri));
        } else {
          events.add(RepositoryEventDto.createForCommit(revForEvent, localRepo, httpConnectionUri));
        }
      }
    }
    return events.build();
  }

  private DiffDto diffRepo(String owner, String repoName, String oldSha1, String newSha1)
      throws IOException, GitAPIException {
    try (Git g = Git.open(gitLocation.getDirectory(owner, repoName))) {
      Repository repo = g.getRepository();
      CanonicalTreeParser oldTreeParser = getTreeParser(oldSha1, repo);
      CanonicalTreeParser newTreeParser = getTreeParser(newSha1, repo);
      List<DiffEntry> diff = g.diff().setOldTree(oldTreeParser).setNewTree(newTreeParser).call();
      return SideBySideDiffFormatter.format(diff, oldSha1, newSha1, 3, repo);
    }
  }

  private static CanonicalTreeParser getTreeParser(String sha1, Repository repo)
      throws IOException {
    CanonicalTreeParser oldTreeParser;
    try (RevWalk walk = new RevWalk(repo)) {
      RevCommit commit = walk.parseCommit(ObjectId.fromString(sha1));
      RevTree tree = walk.parseTree(commit.getTree().getId());
      oldTreeParser = new CanonicalTreeParser();
      try (ObjectReader reader = repo.newObjectReader()) {
        oldTreeParser.reset(reader, tree.getId());
      }
      walk.dispose();
    }
    return oldTreeParser;
  }

  private static RevTree getRevTree(String sha1, Repository repo, RevWalk revWalk) {
    RevTree tree;
    try {
      ObjectId tagId = repo.resolve(Constants.HEAD.equals(sha1) ? Constants.HEAD : sha1);
      if (tagId == null) {
        if (Constants.HEAD.equals(sha1)) {
          throw new IOException("Failed to find HEAD in repo.");
        } else {
          throw new IOException("Failed to find sha1 " + sha1);
        }
      }
      RevCommit revCommit = revWalk.parseCommit(tagId);
      tree = revCommit.getTree();

    } catch (RevisionSyntaxException | IOException e) {
      throw new GitApiError("Failed to load revision for head of repository", e);
    }
    return tree;
  }

  private String resolveHeadSha(String ownerName, String repoName) {
    try {
      return Git.lsRemoteRepository()
          .setRemote(gitLocation.getDirectory(ownerName, repoName).getPath()).setHeads(true).call()
          .stream()
          .filter(ref -> ref.getName().equals("refs/heads/master"))
          .map(Ref::getObjectId)
          .map(ObjectId::getName)
          .findFirst()
          .orElseThrow(
              () -> new RefNotFoundException("Failed to find reference named refs/heads/master"));
    } catch (GitAPIException e) {
      throw new GitApiError("Failed to resolve SHA1 for refs/heads/master", e);
    }
  }

  private Optional<RepositoryEventDto> latestCommit(LocalRepo localRepo) {
    try (Git g = Git.open(gitLocation.getDirectory(localRepo.ownerName(), localRepo.repoName()))) {
      RevCommit latestCommit = g.log().call().iterator().next();
      return Optional.of(
          RepositoryEventDto.builder()
              .message(latestCommit.getShortMessage())
              .sha1(latestCommit.toObjectId().getName())
              .codeLink(
                  httpConnectionUri.url(
                      ChimeRepoController.class,
                      localRepo.ownerName() + "/" + localRepo.repoName() + "/code/"))
              .time(Date.from(Instant.ofEpochSecond(latestCommit.getCommitTime())))
              .build());
    } catch (NoHeadException e) {
      return Optional.empty();
    } catch (IOException | GitAPIException e) {
      throw new GitApiError(e);
    }
  }

  private RepoDto createRepoDto(
      @Context UserDao requestingUser, LocalRepo localRepo, TransactionQueryRunner queryRunner) {
    ChimeTask taskInfo = potteryInterface.getTaskInfo(localRepo.potteryTaskId());

    Submissions submissions = Submissions.lookup(localRepo, taskInfo.actions(), queryRunner);

    ImmutableList<RepoActionDto> repoActions =
        submissions.actions().stream()
            .map(
                action -> {
                  RepoActionDto.Builder repoAction =
                      RepoActionDto.builder()
                          .name(action.name())
                          .description(action.description())
                          .active(action.name().equals(submissions.currentAction().name()));
                  action
                      .latestSubmission()
                      .map(
                          s ->
                              createSubmissionDto(
                                  requestingUser, s, httpConnectionUri, gitLocation))
                      .ifPresent(repoAction::latestSubmission);
                  action
                      .latestPassed()
                      .map(
                          s ->
                              createSubmissionDto(
                                  requestingUser, s, httpConnectionUri, gitLocation))
                      .ifPresent(repoAction::latestPassed);
                  return repoAction.build();
                })
            .collect(toImmutableList());
    RepoDto.Builder builder =
        RepoDto.builder()
            .taskName(taskInfo.name())
            .problemStatement(taskInfo.problemStatement())
            .problemStatementShort(taskInfo.problemStatementShort())
            .localRepo(localRepo)
            .repoUrls(RepoUrls.createNew(requestingUser, localRepo, httpConnectionUri, gitLocation))
            .actions(repoActions);
    latestCommit(localRepo).ifPresent(builder::latestCommit);
    TickDao.lookup(localRepo, queryRunner).ifPresent(builder::tick);
    return builder.build();
  }

  private static SubmissionDto createSubmissionDto(
      @Context UserDao requestingUser,
      SubmissionDao s,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation) {
    return SubmissionDto.builder()
        .submission(s)
        .submissionUrls(SubmissionUrls.createNew(requestingUser, s, httpConnectionUri, gitLocation))
        .build();
  }
}
