package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SshKeyDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@Produces("text/html")
@Path("/ssh_keys")
public class SshKeyController {

  private final TemplateProcessor templateProcessor;
  private final Database database;
  private final Messages messages;
  private final HttpConnectionUri httpConnectionUri;

  @Inject
  public SshKeyController(
      TemplateProcessor templateProcessor,
      Database database,
      Messages messages,
      HttpConnectionUri httpConnectionUri) {
    this.templateProcessor = templateProcessor;
    this.database = database;
    this.messages = messages;
    this.httpConnectionUri = httpConnectionUri;
  }

  @GET
  @Path("/")
  public String listKeys(@Context UserDao user) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      ImmutableList<SshKeyDao> keys = SshKeyDao.lookupKeys(user, q);
      return templateProcessor.process("ssh_keys.ftl", user, ImmutableMap.of("keys", keys));
    }
  }

  @GET
  @Path("/new")
  public String newKey(@Context UserDao user) {
    return templateProcessor.process("ssh_keys_new.ftl", user, ImmutableMap.of());
  }

  @POST
  @Path("/new")
  public Response createKey(@Context UserDao user, @FormParam("key") String key) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      SshKeyDao sshKey = SshKeyDao.createNew(user, key, q);
      if (sshKey.canParse()) {
        messages.postMessage(user, "Stored new SSH key");
        q.commit();
      } else {
        messages.postMessage(user, "Failed to parse key");
        q.rollback();
      }
    }
    return httpConnectionUri.redirectToFrontend(SshKeyController.class, "");
  }

  @POST
  @Path("/delete")
  public Response deleteKey(@Context UserDao user, @FormParam("keyId") Integer keyId) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      SshKeyDao.delete(user, keyId, q);
      q.commit();
    }
    messages.postMessage(user, "Deleted key");
    return httpConnectionUri.redirectToFrontend(SshKeyController.class, "");
  }
}
