package uk.ac.cam.cl.dtg.teaching.chime.urls;

import com.google.auto.value.AutoValue;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeRepoController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.LeaderboardController;

@AutoValue
public abstract class RepoUrls {

  public abstract String fullName();

  public abstract String summary();

  public abstract String timeline();

  public abstract String code();

  public abstract String gitClone();

  public abstract String newSubmission();

  public abstract String delete();

  public abstract String extension();

  public abstract String tickState();

  public abstract String exportSummary();

  public abstract String leaderboard();

  public static Builder builder() {
    return new AutoValue_RepoUrls.Builder();
  }

  public static RepoUrls createNew(
      UserDao requestingUser,
      LocalRepo localRepo,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation) {
    return RepoUrls.builder()
        .fullName(localRepo.ownerName() + "/" + localRepo.repoName())
        .gitClone(
            gitLocation.getSshUri(
                requestingUser.userName(), localRepo.ownerName(), localRepo.repoName()))
        .code(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/code", localRepo.ownerName(), localRepo.repoName())))
        .delete(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/delete", localRepo.ownerName(), localRepo.repoName())))
        .summary(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s", localRepo.ownerName(), localRepo.repoName())))
        .timeline(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/timeline", localRepo.ownerName(), localRepo.repoName())))
        .newSubmission(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/submissions", localRepo.ownerName(), localRepo.repoName())))
        .extension(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/extension", localRepo.ownerName(), localRepo.repoName())))
        .tickState(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/tickstate", localRepo.ownerName(), localRepo.repoName())))
        .exportSummary(
            httpConnectionUri.url(
                ChimeRepoController.class,
                String.format("%s/%s/summary", localRepo.ownerName(), localRepo.repoName())))
        .leaderboard(
            httpConnectionUri.url(
                LeaderboardController.class, String.format("%s", localRepo.potteryTaskId())))
        .build();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder summary(String summary);

    public abstract Builder timeline(String timeline);

    public abstract Builder code(String code);

    public abstract Builder gitClone(String gitClone);

    public abstract Builder delete(String deleteConfirmation);

    public abstract Builder newSubmission(String newSubmission);

    public abstract Builder extension(String extension);

    public abstract Builder tickState(String tickStatus);

    public abstract Builder fullName(String fullName);

    public abstract Builder exportSummary(String exportSummary);

    public abstract Builder leaderboard(String leaderboard);

    public abstract RepoUrls build();
  }
}
