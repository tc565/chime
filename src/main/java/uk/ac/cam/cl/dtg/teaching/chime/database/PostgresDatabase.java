package uk.ac.cam.cl.dtg.teaching.chime.database;

import com.google.inject.Inject;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;
import java.beans.PropertyVetoException;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import javax.inject.Named;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PostgresDatabase implements Database {

  private static final Logger LOG = LoggerFactory.getLogger(PostgresDatabase.class);

  private ComboPooledDataSource connectionPool;

  @Inject
  public PostgresDatabase(
      @Named("jdbcUrl") String jdbcUrl,
      @Named("databaseUser") String databaseUser,
      @Named("databasePassword") String databasePassword) {
    try {
      connectionPool = new ComboPooledDataSource();
      connectionPool.setDriverClass("org.postgresql.Driver");
      connectionPool.setJdbcUrl(jdbcUrl);
      connectionPool.setUser(databaseUser);
      connectionPool.setPassword(databasePassword);
      connectionPool.setTestConnectionOnCheckout(true);
    } catch (PropertyVetoException e) {
      LOG.error("Failed to open database", e);
    }
  }

  @Override
  public TransactionQueryRunner getQueryRunner() {
    return new TransactionQueryRunner(connectionPool) {
      @Override
      public int nextVal(String sequence) {
        return query(
            "select nextval('" + sequence + "')",
            rs -> {
              rs.next();
              return rs.getInt(1);
            });
      }
    };
  }

  @Override
  public void stop() {
    LOG.info("Closing connection pool");
    connectionPool.close();
    try {
      DataSources.destroy(connectionPool);
    } catch (SQLException e) {
      LOG.error("Error destroying connection pool", e);
    }

    String driverClass = connectionPool.getDriverClass();
    Enumeration<Driver> drivers = DriverManager.getDrivers();
    while (drivers.hasMoreElements()) {
      Driver driver = drivers.nextElement();
      if (driver.getClass().getName().equals(driverClass)) {
        try {
          LOG.info("Deregistering {}", driverClass);
          DriverManager.deregisterDriver(driver);
        } catch (SQLException ex) {
          LOG.error("Error deregistering JDBC driver {}", driver, ex);
        }
      }
    }
  }
}
