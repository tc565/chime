package uk.ac.cam.cl.dtg.teaching.chime.urls;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class CommonUrls {

  public abstract String usersTasks();

  public abstract String allTasks();

  public abstract String sshKeys();

  public abstract String research();

  public abstract String progress();

  public abstract String permissions();

  public static Builder builder() {
    return new AutoValue_CommonUrls.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder usersTasks(String usersTasks);

    public abstract Builder allTasks(String allTasks);

    public abstract Builder sshKeys(String sshKeys);

    public abstract Builder research(String leaderBoard);

    public abstract Builder progress(String progress);

    public abstract Builder permissions(String permissions);

    public abstract CommonUrls build();
  }
}
