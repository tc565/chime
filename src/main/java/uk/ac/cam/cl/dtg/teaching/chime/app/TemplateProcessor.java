package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.io.StringWriter;
import javax.inject.Named;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeRepoController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeTasksController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.PermissionsController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ProgressController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ResearchOptInController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.SshKeyController;
import uk.ac.cam.cl.dtg.teaching.chime.urls.CommonUrls;
import uk.ac.cam.cl.dtg.teaching.chime.urls.RepoUrls;

public class TemplateProcessor {

  private final Configuration configuration;
  private final Messages messages;
  private final GitLocation gitLocation;
  private final String urlPrefix;
  private final boolean testingMode;
  private final HttpConnectionUri httpConnectionUri;
  private final String meetingGenerationNonce;

  @Inject
  public TemplateProcessor(
      TemplateConfiguration templateConfiguration,
      @Named("urlPrefix") String urlPrefix,
      @Named("testingMode") boolean testingMode,
      @Named("meetingGenerationNonce") String meetingGenerationNonce,
      Messages messages,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation) {
    this.configuration = templateConfiguration.getConfiguration();
    this.urlPrefix = urlPrefix;
    this.testingMode = testingMode;
    this.meetingGenerationNonce = meetingGenerationNonce;
    this.messages = messages;
    this.httpConnectionUri = httpConnectionUri;
    this.gitLocation = gitLocation;
  }

  private String process(String templateName, ImmutableMap<String, Object> response) {
    try {
      Template template = configuration.getTemplate(templateName);
      StringWriter writer = new StringWriter();
      template.process(response, writer);
      return writer.toString();
    } catch (TemplateException | IOException e) {
      throw new TemplateError(e);
    }
  }

  public String process(
      String templateName, UserDao authenticatedUser, ImmutableMap<String, Object> response) {
    return process(
        templateName,
        ImmutableMap.<String, Object>builder()
            .putAll(response)
            .put("urlPrefix", urlPrefix)
            .put("authenticatedUser", authenticatedUser)
            .put(
                "commonUrls",
                CommonUrls.builder()
                    .allTasks(httpConnectionUri.url(ChimeTasksController.class, ""))
                    .sshKeys(httpConnectionUri.url(SshKeyController.class, ""))
                    .usersTasks(httpConnectionUri.url(ChimeRepoController.class, ""))
                    .research(httpConnectionUri.url(ResearchOptInController.class, ""))
                    .progress(httpConnectionUri.url(ProgressController.class, ""))
                    .permissions(httpConnectionUri.url(PermissionsController.class, ""))
                    .build())
            .put("testingMode", testingMode)
            .put("meetingGenerationNonce", meetingGenerationNonce)
            .put("messages", messages.popMessage(authenticatedUser).orElse(""))
            .build());
  }

  public String process(
      String templateName,
      UserDao authenticatedUser,
      RepoUrls repoUrls,
      ImmutableMap<String, Object> response) {
    return process(
        templateName,
        authenticatedUser,
        ImmutableMap.<String, Object>builder().putAll(response).put("repoUrls", repoUrls).build());
  }
}
