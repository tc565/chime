package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.auto.value.AutoValue;
import java.util.regex.Pattern;

@AutoValue
public abstract class TaskAction {

  public static final Pattern NEVER_MATCH = Pattern.compile("a^");

  public abstract String name();

  public abstract String description();

  /** Additional skeleton files to add to the user's repo when starting this action. */
  public abstract Pattern additionalFileFilter();

  public static TaskAction create(String name, String description, Pattern additionalFileFilter) {
    return new AutoValue_TaskAction(name, description, additionalFileFilter);
  }
}
