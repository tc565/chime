package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;

@AutoValue
public abstract class DiffFileDto {

  public abstract String oldPath();

  public abstract String newPath();

  public abstract ImmutableList<DiffLineDto> lines();

  public boolean containsDeletion() {
    return lines().stream()
        .anyMatch(
            line ->
                line.operation().equals(DiffLineDto.Operation.DELETE)
                    || line.operation().equals(DiffLineDto.Operation.MODIFY));
  }

  public boolean isNewFile() {
    return oldPath().equals("/dev/null");
  }

  public boolean isDeletedFile() {
    return newPath().equals("/dev/null");
  }

  public static Builder builder() {
    return new AutoValue_DiffFileDto.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder oldPath(String oldPath);

    public abstract Builder newPath(String newPath);

    public abstract ImmutableList.Builder<DiffLineDto> linesBuilder();

    public Builder addLine(String original, String replacement, DiffLineDto.Operation operation) {
      linesBuilder().add(DiffLineDto.create(original, replacement, operation));
      return this;
    }

    public abstract DiffFileDto build();
  }
}
