package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import static com.google.common.collect.ImmutableList.toImmutableList;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import de.jkeylockmanager.manager.KeyLockManager;
import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.util.Collection;
import java.util.Comparator;
import java.util.Optional;
import java.util.regex.Pattern;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.StoredConfig;
import uk.ac.cam.cl.dtg.teaching.chime.app.AccessDeniedError;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeFileWriter;
import uk.ac.cam.cl.dtg.teaching.chime.app.ChimeTask;
import uk.ac.cam.cl.dtg.teaching.chime.app.FileUtil;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitApiError;
import uk.ac.cam.cl.dtg.teaching.chime.app.GitLocation;
import uk.ac.cam.cl.dtg.teaching.chime.app.HttpConnectionUri;
import uk.ac.cam.cl.dtg.teaching.chime.app.Messages;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.SkeletonFiles;
import uk.ac.cam.cl.dtg.teaching.chime.app.StringUtil;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskAction;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskError;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.app.Templates;
import uk.ac.cam.cl.dtg.teaching.chime.dao.AssessedExerciseDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.NewLocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.TickDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.chime.dto.TaskDto;
import uk.ac.cam.cl.dtg.teaching.pottery.exceptions.RepoStorageException;
import uk.ac.cam.cl.dtg.teaching.pottery.model.RepoInfoWithStatus;

@Produces("text/html")
@Path("/tasks")
public class ChimeTasksController {

  private static final Pattern INVALID_REPO_NAME_PATTERN = Pattern.compile("[^a-zA-Z0-9-_]");
  public static final String INITIALISED_TASK = "Initialised task";

  private final TemplateProcessor templateProcessor;
  private final PotteryInterface potteryInterface;
  private final Database database;
  private final KeyLockManager keyLockManager;
  private final HttpConnectionUri httpConnectionUri;
  private final GitLocation gitLocation;
  private final Messages messages;

  @Inject
  public ChimeTasksController(
      TemplateProcessor templateProcessor,
      PotteryInterface potteryInterface,
      Database database,
      KeyLockManager keyLockManager,
      HttpConnectionUri httpConnectionUri,
      GitLocation gitLocation,
      Messages messages) {
    this.templateProcessor = templateProcessor;
    this.potteryInterface = potteryInterface;
    this.database = database;
    this.keyLockManager = keyLockManager;
    this.httpConnectionUri = httpConnectionUri;
    this.gitLocation = gitLocation;
    this.messages = messages;
  }

  @GET
  @Path("/")
  public String listAll(@Context UserDao user) {
    if (!user.isAdmin()) {
      throw new AccessDeniedError("Only admins can view this page.");
    }
    Collection<TaskDto> taskList =
        potteryInterface.listTasks().stream()
            .map(taskInfo -> TaskDto.fromTaskInfo(taskInfo, httpConnectionUri))
            .sorted(Comparator.comparing(TaskDto::name))
            .collect(toImmutableList());
    return templateProcessor.process("task_list.ftl", user, ImmutableMap.of("taskList", taskList));
  }

  @GET
  @Path("/{taskId}")
  public Response taskInfo(@Context UserDao user, @PathParam("taskId") String taskId) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      ImmutableList<LocalRepo> existing = LocalRepo.lookupByTask(user, taskId, q);
      if (existing.size() == 1) {
        return httpConnectionUri.redirectToFrontend(
            ChimeRepoController.class, user.userName() + "/" + existing.get(0).repoName());
      }
      if (existing.size() > 1) {
        messages.postMessage(user, "You have already started this task more than once");
        return httpConnectionUri.redirectToFrontend(ChimeRepoController.class, user.userName());
      }
    }

    TaskDto taskDto = TaskDto.fromTaskInfo(potteryInterface.getTaskInfo(taskId), httpConnectionUri);
    return Response.ok()
        .entity(
            templateProcessor.process(
                "task_start.ftl",
                user,
                ImmutableMap.of(
                    "task", taskDto, "defaultName", StringUtil.fileFriendly(taskDto.name()))))
        .type(MediaType.TEXT_HTML_TYPE)
        .build();
  }

  @POST
  @Path("/{taskId}")
  public Response start(
      @Context UserDao user, @PathParam("taskId") String taskId, @FormParam("name") String repoName)
      throws RepoStorageException {

    if (repoName == null || INVALID_REPO_NAME_PATTERN.matcher(repoName).find()) {
      throw new RepoStorageException(
          "Invalid name (only letters, numbers, hyphens and underscores are allowed");
    }

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      ImmutableList<LocalRepo> existing = LocalRepo.lookupByTask(user, taskId, q);
      if (existing.size() == 1) {
        return httpConnectionUri.redirectToFrontend(
            ChimeRepoController.class, user.userName() + "/" + existing.get(0).repoName());
      }
      if (existing.size() > 1) {
        messages.postMessage(user, "You have already started this task more than once");
        return httpConnectionUri.redirectToFrontend(ChimeRepoController.class, user.userName());
      }
    }

    keyLockManager.executeLocked(
        user.userId() + "/" + taskId, () -> createRepo(user, taskId, repoName));

    return httpConnectionUri.redirectToFrontend(
        ChimeRepoController.class, user.userName() + "/" + repoName);
  }

  private void createRepo(UserDao user, String taskId, String repoName) {
    ChimeTask taskInfo = potteryInterface.getTaskInfo(taskId);

    String variant = Iterables.getOnlyElement(taskInfo.variants());

    String url = gitLocation.getPotterySshUri(user.userName(), repoName);
    File repoLocation = gitLocation.getDirectory(user.userName(), repoName);
    if (repoLocation.exists()) {
      throw new GitApiError(
          "User " + user.userName() + " already has a repository named " + repoName);
    }

    String action =
        taskInfo.actions().stream()
            .findFirst()
            .map(TaskAction::name)
            .orElseThrow(() -> new TaskError("No actions found in task"));

    try (FileUtil.AutoDelete autoDelete = FileUtil.mkdirWithAutoDelete(repoLocation);
        Git g = Git.init().setDirectory(repoLocation).call();
        TransactionQueryRunner q = database.getQueryRunner()) {
      // TODO(acr31) this ought to pull updates onto the current branch but it doesn't do anything
      StoredConfig config = g.getRepository().getConfig();
      config.setString("receive", null, "denyCurrentBranch", "updateInstead");
      config.save();
      RepoInfoWithStatus repoInfo =
          potteryInterface.createRemoteRepo(taskInfo.potteryTaskId(), variant, url);
      LocalRepo localRepo =
          NewLocalRepo.builder()
              .user(user)
              .ownerName(user.userName())
              .repoName(repoName)
              .potteryTaskId(taskId)
              .variant(variant)
              .potteryRepoId(repoInfo.getRepoId())
              .startTime(Instant.now())
              .build()
              .insert(q);

      ChimeFileWriter fileWriter = new ChimeFileWriter(repoLocation, g);
      SkeletonFiles.updateSkeletonFiles(
          user,
          taskInfo,
          action,
          () -> potteryInterface.getSkeletonFileNames(localRepo),
          skeletonFileName -> potteryInterface.readSkeletonFile(taskId, variant, skeletonFileName),
          fileWriter);
      fileWriter.write("LICENSE", Templates.getLicenseFile());
      fileWriter.write("NOTICE", Templates.getNotice(taskInfo, user));
      g.commit().setMessage(INITIALISED_TASK).call();

      Optional<AssessedExerciseDao> assessedExercise =
          AssessedExerciseDao.lookup(taskInfo.potteryTaskId(), q);
      assessedExercise.ifPresent(
          a ->
              TickDao.builder()
                  .localRepo(localRepo)
                  .tickState(TickDao.TickState.NOT_ASSESSED)
                  .assessedBy(TickDao.NO_ASSESSOR)
                  .deadline(a.deadline())
                  .sha1("n/a")
                  .online(a.online())
                  .build()
                  .insert(q));

      q.commit();
      autoDelete.persist();

    } catch (GitAPIException | IOException e) {
      throw new GitApiError(e);
    }
  }
}
