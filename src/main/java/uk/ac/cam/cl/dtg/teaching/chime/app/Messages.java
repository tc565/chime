package uk.ac.cam.cl.dtg.teaching.chime.app;

import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;

public class Messages {

  private ConcurrentHashMap<UserDao, String> messages;

  public Messages() {
    messages = new ConcurrentHashMap<>();
  }

  public void postMessage(UserDao user, String message) {
    messages.put(user, message);
  }

  public Optional<String> popMessage(UserDao user) {
    return Optional.ofNullable(messages.remove(user));
  }
}
