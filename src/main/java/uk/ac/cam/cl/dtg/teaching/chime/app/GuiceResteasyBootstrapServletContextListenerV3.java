package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.inject.Injector;
import com.google.inject.Module;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletContext;
import javax.servlet.annotation.WebListener;
import org.jboss.resteasy.plugins.guice.GuiceResteasyBootstrapServletContextListener;

@WebListener
public class GuiceResteasyBootstrapServletContextListenerV3
    extends GuiceResteasyBootstrapServletContextListener {

  private static Injector injector;

  public static Injector getInjector() {
    return injector;
  }

  @Override
  protected List<? extends Module> getModules(ServletContext context) {
    return Arrays.asList(new ApplicationModule(context));
  }

  @Override
  protected void withInjector(Injector i) {
    injector = i;
  }
}
