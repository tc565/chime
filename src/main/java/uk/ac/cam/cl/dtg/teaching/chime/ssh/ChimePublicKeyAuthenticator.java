package uk.ac.cam.cl.dtg.teaching.chime.ssh;

import com.google.common.collect.ImmutableList;
import java.security.GeneralSecurityException;
import java.security.PublicKey;
import org.apache.sshd.server.auth.AsyncAuthException;
import org.apache.sshd.server.auth.pubkey.PublickeyAuthenticator;
import org.apache.sshd.server.session.ServerSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SshKeyDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.DatabaseError;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

class ChimePublicKeyAuthenticator implements PublickeyAuthenticator {

  private static final Logger logger = LoggerFactory.getLogger(ChimePublicKeyAuthenticator.class);

  private final Database database;
  private final PublicKey potteryPublicKey;

  ChimePublicKeyAuthenticator(Database database, PublicKey potteryPublicKey) {
    this.database = database;
    this.potteryPublicKey = potteryPublicKey;
  }

  @Override
  public boolean authenticate(String username, PublicKey key, ServerSession session)
      throws AsyncAuthException {

    if (username.equals(SshDaemon.POTTERY_USER)) {
      boolean equals = key.equals(potteryPublicKey);
      if (equals) {
        logger.info("Authenticated {}", SshDaemon.POTTERY_USER);
      } else {
        logger.info("Rejected {}", SshDaemon.POTTERY_USER);
      }
      return equals;
    }

    ImmutableList<SshKeyDao> keys = getKeys(username);
    for (SshKeyDao sshKey : keys) {
      try {
        if (sshKey.toPublicKey().equals(key)) {
          logger.info("Authenticated " + username);
          return true;
        }
      } catch (GeneralSecurityException e) {
        logger.error("Failed to parse key " + sshKey.key());
      }
    }
    logger.info("Rejected " + username);
    return false;
  }

  private ImmutableList<SshKeyDao> getKeys(String username) {
    try (TransactionQueryRunner q = database.getQueryRunner()) {
      return UserDao.lookup(username, q)
          .map(user -> getSshKeys(user, q))
          .orElse(ImmutableList.of());
    } catch (DatabaseError e) {
      logger.error("Failed to lookup " + username + " in database", e);
      return ImmutableList.of();
    }
  }

  private static ImmutableList<SshKeyDao> getSshKeys(UserDao user, TransactionQueryRunner q) {
    try {
      return SshKeyDao.lookupKeys(user, q);
    } catch (DatabaseError e) {
      logger.error("Failed to lookup ssh keys in database", e);
      return ImmutableList.of();
    }
  }
}
