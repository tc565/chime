package uk.ac.cam.cl.dtg.teaching.chime.kind;

import static uk.ac.cam.cl.dtg.teaching.chime.kind.StatusInOutput.getStepDaos;
import static uk.ac.cam.cl.dtg.teaching.chime.kind.StatusInOutput.updateSubmissionDao;

import com.google.common.collect.ImmutableList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.StepDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;
import uk.ac.cam.cl.dtg.teaching.pottery.model.Submission;

public class FurtherJavaTick0 implements TaskKind {

  @Override
  public SubmissionDao processSubmission(
      int submissionId,
      LocalRepo localRepo,
      Date submissionTime,
      Submission s,
      TransactionQueryRunner queryRunner,
      PotteryInterface potteryInterface) {
    ImmutableList<StepDao> steps = getStepDaos(localRepo, s, potteryInterface);
    steps.stream()
        .filter(step -> step.name().equals("validator"))
        .findFirst()
        .ifPresent(
            step -> parseFurtherJavaTick0(localRepo.user().userId(), step.output(), queryRunner));
    return updateSubmissionDao(submissionId, localRepo, submissionTime, s, steps);
  }

  public static void parseFurtherJavaTick0(int userid, String output, TransactionQueryRunner q) {
    if (!output.trim().endsWith("*** PASSED ALL TESTS ***")) {
      return;
    }

    Pattern p = Pattern.compile("^! Time-[0-9]+=([0-9]+)", Pattern.MULTILINE);
    Matcher m = p.matcher(output);
    int start = 0;
    long totalTime = 0;
    while (m.find(start)) {
      totalTime += Long.parseLong(m.group(1));
      start = m.end();
    }

    boolean foundRow =
        q.update("UPDATE leaderboard set timeTaken = ? where userid = ?", totalTime, userid) != 0;
    if (!foundRow) {
      q.insert("INSERT into leaderboard(userid,timetaken) values (?,?)", userid, totalTime);
    }
  }
}
