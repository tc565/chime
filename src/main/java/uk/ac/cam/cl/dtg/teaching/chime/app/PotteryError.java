package uk.ac.cam.cl.dtg.teaching.chime.app;

public class PotteryError extends RuntimeException {

  public PotteryError(Throwable cause) {
    super(cause);
  }
}
