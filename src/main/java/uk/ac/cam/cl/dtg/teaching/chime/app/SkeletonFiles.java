package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.common.collect.ImmutableList;
import com.google.protobuf.ByteString;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.apache.tika.Tika;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;

public class SkeletonFiles {

  private static final Logger logger = LoggerFactory.getLogger(SkeletonFiles.class);

  public static void updateSkeletonFiles(
      UserDao user,
      ChimeTask taskInfo,
      String currentAction,
      Supplier<List<String>> skeletonFileNamesSupplier,
      Function<String, ByteString> skeletonFileSupplier,
      ChimeFileWriter fileWriter) {
    List<String> skeletonFileNames = skeletonFileNamesSupplier.get();

    // eliminate all files which match any future action
    ImmutableList<TaskAction> taskActions = taskInfo.followingActions(currentAction);
    for (TaskAction action : taskActions) {
      if (action.additionalFileFilter() == TaskAction.NEVER_MATCH) {
        continue;
      }
      skeletonFileNames =
          skeletonFileNames.stream()
              .filter(n -> !action.additionalFileFilter().matcher(n).find())
              .collect(Collectors.toList());
    }
    String header = Templates.getJavaHeader(taskInfo, user);
    for (String skeletonFile : skeletonFileNames) {
      ByteString data = skeletonFileSupplier.apply(skeletonFile);
      String crsidReplacementString = taskInfo.crsidReplacementString();
      skeletonFile = skeletonFile.replace(crsidReplacementString, user.userName());
      if (isTextFormat(skeletonFile, data)) {
        String fileContents = data.toString(StandardCharsets.UTF_8);
        fileContents = fileContents.replace(crsidReplacementString, user.userName());
        if (skeletonFile.endsWith(".java")) {
          fileContents = header + fileContents;
        }
        data = ByteString.copyFrom(fileContents, StandardCharsets.UTF_8);
      }
      fileWriter.write(skeletonFile, data);
    }
  }

  private static boolean isTextFormat(String filename, ByteString contents) {
    if (filename.endsWith(".txt")
        || filename.endsWith(".java")
        || filename.endsWith(".xml")
        || filename.endsWith(".pl")
        || filename.endsWith(".gitignore")) {
      return true;
    }
    if (filename.endsWith(".bin") || filename.endsWith(".zip") || filename.endsWith(".png")) {
      return false;
    }
    logger.warn("Fall back to Tika for filetype detection for {}", filename);
    String mimeType = new Tika().detect(contents.toByteArray(), filename);

    return mimeType.startsWith("text/") || mimeType.equals("application/xml");
  }
}
