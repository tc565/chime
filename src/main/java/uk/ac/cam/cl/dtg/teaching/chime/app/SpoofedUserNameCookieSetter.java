package uk.ac.cam.cl.dtg.teaching.chime.app;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.NewCookie;
import javax.ws.rs.ext.Provider;
import org.jboss.resteasy.annotations.interception.ServerInterceptor;

@Provider
@ServerInterceptor
public class SpoofedUserNameCookieSetter implements ContainerResponseFilter {

  private final boolean mockRavenAuth;

  @Inject
  public SpoofedUserNameCookieSetter(@Named("mockRavenAuth") boolean mockRavenAuth) {
    this.mockRavenAuth = mockRavenAuth;
  }

  @Override
  public void filter(
      ContainerRequestContext requestContext, ContainerResponseContext responseContext) {
    if (mockRavenAuth) {
      AuthenticationPrincipalInterceptor.getUserNameFromRequest(requestContext)
          .ifPresent(
              userName ->
                  responseContext
                      .getHeaders()
                      .add(
                          "Set-Cookie",
                          new NewCookie(
                              AuthenticationPrincipalInterceptor.SPOOFED_USER_NAME,
                              userName,
                              /* path= */ "/",
                              /* domain= */ null,
                              /* comment= */ null,
                              -1,
                              /* secure= */ false)));
    }
  }
}
