package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.common.collect.ImmutableList;
import com.google.inject.Inject;
import java.net.URI;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.inject.Named;
import javax.servlet.annotation.WebServlet;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

public class HttpConnectionUri {

  private final String urlPrefix;
  private final String apiPrefix;

  @Inject
  public HttpConnectionUri(
      @Named("urlPrefix") String urlPrefix, HttpServletDispatcherV3 httpServletDispatcherV3) {
    this(
        urlPrefix,
        Arrays.stream(
                httpServletDispatcherV3.getClass().getAnnotation(WebServlet.class).initParams())
            .filter(param -> param.name().equals("resteasy.servlet.mapping.prefix"))
            .findFirst()
            .orElseThrow(() -> new RuntimeException("Failed to understand prefix"))
            .value());
  }

  public HttpConnectionUri(String urlPrefix, String apiPrefix) {
    this.urlPrefix = urlPrefix;
    this.apiPrefix = apiPrefix;
  }

  public Response redirectToFrontend(Class<?> frontend, Object path) {
    return Response.seeOther(URI.create(url(frontend, path))).build();
  }

  public String url(Class<?> frontend, Object path) {
    String frontendPath = frontend.getAnnotation(Path.class).value();
    return ImmutableList.of(urlPrefix, apiPrefix, frontendPath, String.valueOf(path)).stream()
        .map(e -> e.replaceAll("^/", "").replaceAll("/$", ""))
        .collect(Collectors.joining("/"));
  }
}
