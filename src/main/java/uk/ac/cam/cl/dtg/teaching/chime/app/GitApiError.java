package uk.ac.cam.cl.dtg.teaching.chime.app;

public class GitApiError extends RuntimeException {

  public GitApiError(String message) {
    super(message);
  }

  public GitApiError(String message, Throwable cause) {
    super(message, cause);
  }

  public GitApiError(Throwable cause) {
    super(cause);
  }
}
