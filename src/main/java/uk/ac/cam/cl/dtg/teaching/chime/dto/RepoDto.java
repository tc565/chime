package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.app.RepoError;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.dao.SubmissionDao;
import uk.ac.cam.cl.dtg.teaching.chime.dao.TickDao;
import uk.ac.cam.cl.dtg.teaching.chime.urls.RepoUrls;

@AutoValue
public abstract class RepoDto {

  public static final String STATUS_TEST_IN_PROGRESS = "TEST_IN_PROGRESS";
  public static final String STATUS_PASSED_ALL_ACTIONS = "PASSED_ALL";
  public static final String STATUS_LATEST_TEST_FAILED = "LATEST_TEST_FAILED";
  public static final String STATUS_WORKING = "WORKING";

  public abstract String taskName();

  public abstract LocalRepo localRepo();

  public abstract String problemStatement();

  public abstract String problemStatementShort();

  public abstract RepoUrls repoUrls();

  public abstract Optional<TickDao> tick();

  public abstract Optional<RepositoryEventDto> latestCommit();

  public abstract ImmutableList<RepoActionDto> actions();

  public RepoActionDto lastAction() {
    return Iterables.getLast(actions());
  }

  public String overallStatus() {
    RepoActionDto lastAction = Iterables.getLast(actions());
    if (lastAction.latestPassed().isPresent()) {
      return STATUS_PASSED_ALL_ACTIONS;
    }
    RepoActionDto activeAction =
        actions().stream()
            .filter(RepoActionDto::active)
            .findFirst()
            .orElseThrow(() -> new RepoError("One action must be active"));
    if (activeAction.latestSubmission().isPresent()) {
      switch (activeAction.latestSubmission().get().submission().status()) {
        case SubmissionDao.STATUS_FAILED:
          return STATUS_LATEST_TEST_FAILED;
        case SubmissionDao.STATUS_PENDING:
        case SubmissionDao.STATUS_RUNNING:
        default:
          return STATUS_TEST_IN_PROGRESS;
      }
    }
    return STATUS_WORKING;
  }

  public static Builder builder() {
    return new AutoValue_RepoDto.Builder();
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder taskName(String taskName);

    public abstract Builder problemStatementShort(String problemStatementShort);

    public abstract Builder problemStatement(String problemStatement);

    public abstract Builder tick(TickDao tick);

    public abstract Builder localRepo(LocalRepo localRepo);

    public abstract Builder repoUrls(RepoUrls repoUrls);

    public abstract Builder latestCommit(RepositoryEventDto latestCommit);

    public abstract Builder actions(ImmutableList<RepoActionDto> actions);

    public abstract RepoDto build();
  }
}
