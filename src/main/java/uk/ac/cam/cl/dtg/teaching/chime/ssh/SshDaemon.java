package uk.ac.cam.cl.dtg.teaching.chime.ssh;

import com.google.inject.Inject;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PublicKey;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.inject.Named;
import org.apache.sshd.common.config.keys.PublicKeyEntry;
import org.apache.sshd.common.keyprovider.KeyPairProvider;
import org.apache.sshd.common.util.security.SecurityUtils;
import org.apache.sshd.git.pack.GitPackCommandFactory;
import org.apache.sshd.server.SshServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.Stoppable;
import uk.ac.cam.cl.dtg.teaching.chime.dao.ConfigDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

public class SshDaemon implements Stoppable {

  private static final Logger logger = LoggerFactory.getLogger(SshDaemon.class);

  public static final String POTTERY_USER = "pottery";
  public static final String PUBLIC_KEY = "publicKey";
  public static final String PRIVATE_KEY = "privateKey";

  private final int port;
  private final String gitRepoRootDir;
  private final PotteryInterface potteryInterface;
  private final Database database;

  private SshServer sshd;

  private final AtomicBoolean isRunning;

  @Inject
  SshDaemon(
      @Named("sshBindPort") int port,
      @Named("gitRepoRootDir") String gitRepoRootDir,
      PotteryInterface potteryInterface,
      Database database) {
    this.port = port;
    this.gitRepoRootDir = gitRepoRootDir;
    this.potteryInterface = potteryInterface;
    this.database = database;

    this.isRunning = new AtomicBoolean(false);
  }

  public void start() {
    sshd = SshServer.setUpDefaultServer();
    sshd.setPort(port);
    KeyPair kp = makeKeyPair(database);
    sshd.setKeyPairProvider(KeyPairProvider.wrap(kp));
    PublicKey potteryKey = requestPublicKey();
    logger.info("Got public key from pottery server");
    sshd.setPublickeyAuthenticator(new ChimePublicKeyAuthenticator(database, potteryKey));
    sshd.setCommandFactory(
        new GitPackCommandFactory(new ChimeGitLocationResolver(gitRepoRootDir, database)));
    SecurityUtils.isBouncyCastleRegistered();
    logger.info("Starting SSH Daemon on port " + sshd.getPort());
    Thread t =
        new Thread(
            () -> {
              try {
                sshd.start();
              } catch (IOException e) {
                throw new Error(e);
              } finally {
                isRunning.set(false);
              }
            });
    t.setDaemon(true);
    t.start();
    isRunning.set(true);
  }

  private PublicKey requestPublicKey() {
    String potteryPublicKey = potteryInterface.publicKey();
    PublicKeyEntry publicKeyEntry = PublicKeyEntry.parsePublicKeyEntry(potteryPublicKey);
    try {
      return publicKeyEntry.resolvePublicKey(null, null, null);
    } catch (IOException | GeneralSecurityException e) {
      throw new RuntimeException(e);
    }
  }

  public boolean isRunning() {
    return isRunning.get();
  }

  private static KeyPair makeKeyPair(Database database) {

    try (TransactionQueryRunner q = database.getQueryRunner()) {
      Optional<ConfigDao> publicKey = ConfigDao.lookup(PUBLIC_KEY, q);
      Optional<ConfigDao> privateKey = ConfigDao.lookup(PRIVATE_KEY, q);

      if (!publicKey.isPresent() || !privateKey.isPresent()) {
        KeyPairGenerator generator = SecurityUtils.getKeyPairGenerator("RSA");
        generator.initialize(2048);
        KeyPair kp = generator.generateKeyPair();
        Base64.Encoder encoder = Base64.getEncoder();
        String privateKeyBase64 = encoder.encodeToString(kp.getPrivate().getEncoded());
        String publicKeyBase64 = encoder.encodeToString(kp.getPublic().getEncoded());
        ConfigDao.create(PUBLIC_KEY, publicKeyBase64).insert(q);
        ConfigDao.create(PRIVATE_KEY, privateKeyBase64).insert(q);
        q.commit();
        return kp;
      } else {
        KeyFactory factory = KeyFactory.getInstance("RSA");
        Base64.Decoder decoder = Base64.getDecoder();
        return new KeyPair(
            factory.generatePublic(new X509EncodedKeySpec(decoder.decode(publicKey.get().value()))),
            factory.generatePrivate(
                new PKCS8EncodedKeySpec(decoder.decode(privateKey.get().value()))));
      }

    } catch (GeneralSecurityException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public void stop() {
    logger.info("Shutting down SshDaemon");
    try {
      sshd.stop(true);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
