package uk.ac.cam.cl.dtg.teaching.chime.frontend;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.google.inject.Inject;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import uk.ac.cam.cl.dtg.teaching.chime.app.PotteryInterface;
import uk.ac.cam.cl.dtg.teaching.chime.app.TemplateProcessor;
import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.TransactionQueryRunner;

@Produces("text/html")
@Path("/leaderboard")
public class LeaderboardController {

  private final Database database;
  private final TemplateProcessor templateProcessor;
  private final PotteryInterface potteryInterface;

  @Inject
  public LeaderboardController(
      Database database, TemplateProcessor templateProcessor, PotteryInterface potteryInterface) {
    this.database = database;
    this.templateProcessor = templateProcessor;
    this.potteryInterface = potteryInterface;
  }

  private static class Row implements Comparable<Row> {
    String username;
    String score;
    Date timeRecorded;
    int role;
    int ordering;

    private Row(String username, String score, Date timeRecorded, int role, int ordering) {
      this.username = username;
      this.score = score;
      this.timeRecorded = timeRecorded;
      this.role = role;
      this.ordering = ordering;
    }

    @Override
    public int compareTo(Row o) {
      return Comparator.<Row, Integer>comparing(r -> r.ordering)
          .thenComparing(r -> r.timeRecorded)
          .thenComparing(r -> r.username)
          .compare(this, o);
    }
  }

  @GET
  @Path("/{potteryTaskId}")
  public String show(
      @Context UserDao requestingUser, @PathParam("potteryTaskId") String potteryTaskId) {

    try (TransactionQueryRunner q = database.getQueryRunner()) {

      Date d = new Date(121, Calendar.APRIL, 27, 9, 0, 0);
      ImmutableList.Builder<Row> builder = ImmutableList.builder();
      builder.add(
          new Row("AndySloaneAgent", "27.6%", d, -1, 724),
          new Row("DoNothingAgent", "0.9%", d, -1, 991),
          new Row("GlennHartmannAgent", "20.2%", d, -1, 798),
          new Row("MichalAgent", "9.6%", d, -1, 904),
          new Row("RandomAgent", "13.1%", d, -1, 869),
          new Row("RobinBaumgartenAgent", "67.3%", d, -1, 327),
          new Row("SergeyKarakovskiyAgent", "22.6%", d, -1, 774),
          new Row("SergeyPolikarpovAgent", "17.0%", d, -1, 830),
          new Row("SpencerShumannAgent", "22.6%", d, -1, 774),
          new Row("TrondEllingsen", "19.0%", d, -1, 810));
      q.query(
          "SELECT username,score,timerecorded,role,ordering from users, leaderboard where "
              + "users.userid = leaderboard.userid order by ordering asc, timerecorded asc",
          rs -> {
            while (rs.next()) {
              builder.add(
                  new Row(
                      rs.getString("username"),
                      rs.getString("score"),
                      new Date(rs.getTimestamp("timerecorded").getTime()),
                      rs.getInt("role"),
                      rs.getInt("ordering")));
            }
            return null;
          });

      ImmutableList.Builder<Result> results = ImmutableList.builder();
      int counter = 1;
      for (Row r : ImmutableList.sortedCopyOf(builder.build())) {
        if (r.role > 0) {
          continue;
        }
        results.add(Result.create(counter, r.username, r.score, r.timeRecorded, r.role == -1));
        if (r.role != -1) {
          counter++;
        }
      }

      return templateProcessor.process(
          "leaderboard.ftl", requestingUser, ImmutableMap.of("results", results.build()));
    }
  }

  @AutoValue
  public abstract static class Result {

    public abstract int rank();

    public abstract String username();

    public abstract String score();

    public abstract Date timeRecorded();

    public abstract boolean reference();

    public static Result create(
        int rank, String username, String score, Date timeRecorded, boolean reference) {
      return new AutoValue_LeaderboardController_Result(
          rank, username, score, timeRecorded, reference);
    }
  }
}
