package uk.ac.cam.cl.dtg.teaching.chime.app;

import java.io.File;
import javax.inject.Inject;
import javax.inject.Named;
import uk.ac.cam.cl.dtg.teaching.chime.dao.LocalRepo;
import uk.ac.cam.cl.dtg.teaching.chime.ssh.SshDaemon;

public class GitLocation {

  private final String sshServer;
  private final int port;
  private final String gitRepoRootDir;

  @Inject
  public GitLocation(
      @Named("sshServer") String sshServer,
      @Named("sshAdvertisedPort") int port,
      @Named("gitRepoRootDir") String gitRepoRootDir) {

    this.sshServer = sshServer;
    this.port = port;
    this.gitRepoRootDir = gitRepoRootDir;
  }

  public String getPotterySshUri(String ownerName, String repoName) {
    return getSshUri(SshDaemon.POTTERY_USER, ownerName, repoName);
  }

  public String getSshUri(String requestingUser, String ownerName, String repoName) {
    if (port == 22) {
      return String.format("%s@%s:%s/%s", requestingUser, sshServer, ownerName, repoName);
    }
    return String.format(
        "ssh://%s@%s:%d/%s/%s", requestingUser, sshServer, port, ownerName, repoName);
  }

  public File getDirectory(String ownerName, String repoName) {
    return new File(new File(gitRepoRootDir, ownerName), repoName);
  }

  public File getDirectory(LocalRepo repo) {
    return getDirectory(repo.ownerName(), repo.repoName());
  }
}
