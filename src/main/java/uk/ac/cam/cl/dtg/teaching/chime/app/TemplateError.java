package uk.ac.cam.cl.dtg.teaching.chime.app;

public class TemplateError extends RuntimeException {
  public TemplateError(Throwable cause) {
    super(cause);
  }
}
