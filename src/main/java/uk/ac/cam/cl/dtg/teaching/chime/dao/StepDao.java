package uk.ac.cam.cl.dtg.teaching.chime.dao;

import com.google.auto.value.AutoValue;
import java.time.Duration;

@AutoValue
public abstract class StepDao {

  public abstract String name();

  public abstract String status();

  public abstract String output();

  public abstract Duration runtime();

  public abstract long ordinal();

  public static StepDao create(
      String name, String status, String output, Duration runtime, long ordinal) {
    return new AutoValue_StepDao(name, status, output, runtime, ordinal);
  }
}
