package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;

@AutoValue
public abstract class FileDto {

  public abstract String fileName();

  public abstract String contents();

  public abstract String brush();

  public static FileDto create(String fileName, String contents, String brush) {
    return new AutoValue_FileDto(fileName, contents, brush);
  }
}
