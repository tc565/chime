package uk.ac.cam.cl.dtg.teaching.chime.dto;

import com.google.auto.value.AutoValue;
import com.google.common.collect.ImmutableList;
import java.util.Optional;
import uk.ac.cam.cl.dtg.teaching.chime.app.TaskAction;

@AutoValue
public abstract class RepoActionDto {

  public abstract String name();

  public abstract String description();

  public abstract Optional<SubmissionDto> latestSubmission();

  public abstract Optional<SubmissionDto> latestPassed();

  public Optional<SubmissionDto> activeSubmission() {
    return active() ? latestSubmission() : latestPassed();
  }

  public abstract boolean active();

  public DiffDto getDiff(ImmutableList<DiffDto> diffs) {
    return activeSubmission()
        .flatMap(
            s ->
                diffs.stream()
                    .filter(diff -> diff.replacementSha1().equals(s.submission().sha1()))
                    .findFirst())
        .orElse(DiffDto.builder().originalSha1("UNKNOWN").replacementSha1("UNKNOWN").build());
  }

  public static Builder builder() {
    return new AutoValue_RepoActionDto.Builder();
  }

  public static Builder builder(TaskAction actionDto) {
    return builder().name(actionDto.name()).description(actionDto.description());
  }

  @AutoValue.Builder
  public abstract static class Builder {

    public abstract Builder name(String name);

    public abstract Builder description(String description);

    public abstract Builder latestSubmission(SubmissionDto latestSubmission);

    public abstract Builder latestPassed(SubmissionDto latestPassed);

    public abstract Builder active(boolean active);

    public abstract RepoActionDto build();
  }
}
