package uk.ac.cam.cl.dtg.teaching.chime.database;

public class DatabaseError extends RuntimeException {

  public DatabaseError(String message, Throwable cause) {
    super(message, cause);
  }

  DatabaseError(Throwable e) {
    super(e);
  }

  public DatabaseError(String message) {
    super(message);
  }
}
