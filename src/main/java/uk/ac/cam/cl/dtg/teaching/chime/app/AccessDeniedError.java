package uk.ac.cam.cl.dtg.teaching.chime.app;

import uk.ac.cam.cl.dtg.teaching.chime.dao.UserDao;

public class AccessDeniedError extends RuntimeException {

  public AccessDeniedError(String string) {
    super(string);
  }

  public AccessDeniedError(UserDao requestingUser, UserDao ownerUser) {
    super(
        "User "
            + requestingUser.userName()
            + " cannot access resources owned by "
            + ownerUser.userName());
  }
}
