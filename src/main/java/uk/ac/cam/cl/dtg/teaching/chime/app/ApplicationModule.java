package uk.ac.cam.cl.dtg.teaching.chime.app;

import com.google.inject.Binder;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Names;
import de.jkeylockmanager.manager.KeyLockManager;
import de.jkeylockmanager.manager.KeyLockManagers;
import java.util.Enumeration;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.servlet.ServletContext;
import uk.ac.cam.cl.dtg.teaching.chime.database.Database;
import uk.ac.cam.cl.dtg.teaching.chime.database.InMemoryDatabase;
import uk.ac.cam.cl.dtg.teaching.chime.database.PostgresDatabase;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeRepoController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeSubmissionsController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ChimeTasksController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.LeaderboardController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.PermissionsController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ProgressController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.ResearchOptInController;
import uk.ac.cam.cl.dtg.teaching.chime.frontend.SshKeyController;
import uk.ac.cam.cl.dtg.teaching.chime.ssh.SshDaemon;
import uk.ac.cam.cl.dtg.teaching.exceptions.ExceptionHandler;
import uk.ac.cam.cl.dtg.teaching.exceptions.RemoteFailureHandler;
import uk.ac.cam.cl.dtg.teaching.pottery.api.PotteryBackend;
import uk.ac.cam.cl.dtg.teaching.pottery.api.PotteryBackendImpl;
import uk.ac.cam.cl.dtg.teaching.pottery.api.mock.PotteryBackendMock;

public class ApplicationModule implements Module {

  private final ServletContext context;

  public ApplicationModule(ServletContext context) {
    this.context = context;
  }

  @Override
  public void configure(Binder binder) {
    binder.bind(ServletContext.class).toProvider(() -> context);
    binder.bind(AuthenticationPrincipalInterceptor.class);
    binder.bind(SpoofedUserNameCookieSetter.class);
    binder.bind(ChimeRepoController.class);
    binder.bind(SshKeyController.class);
    binder.bind(ChimeTasksController.class);
    binder.bind(ChimeSubmissionsController.class);
    binder.bind(PermissionsController.class);
    binder.bind(ExceptionHandler.class);
    binder.bind(RemoteFailureHandler.class);
    binder.bind(HttpConnectionUri.class);
    binder.bind(GitLocation.class);
    binder.bind(TemplateProcessor.class).in(Singleton.class);
    binder.bind(SshDaemon.class).asEagerSingleton();
    binder.bind(PollSubmissionWorker.class).asEagerSingleton();
    binder.bind(LeaderboardController.class);
    binder.bind(ProgressController.class);
    binder.bind(ResearchOptInController.class);
    binder.bind(Formatter.class);

    boolean mockDatabase = Boolean.parseBoolean(context.getInitParameter("mockDatabase"));
    binder
        .bind(Database.class)
        .to(mockDatabase ? InMemoryDatabase.class : PostgresDatabase.class)
        .in(Singleton.class);
    binder.bind(Messages.class).in(Singleton.class);
    boolean mockPotteryBackend =
        Boolean.parseBoolean(context.getInitParameter("mockPotteryBackend"));
    binder
        .bind(PotteryBackend.class)
        .to(mockPotteryBackend ? PotteryBackendMock.class : PotteryBackendImpl.class)
        .in(Singleton.class);

    Enumeration<String> names = context.getInitParameterNames();
    while (names.hasMoreElements()) {
      String name = names.nextElement();
      binder.bindConstant().annotatedWith(Names.named(name)).to(context.getInitParameter(name));
    }

    boolean mockRavenAuth = Boolean.parseBoolean(context.getInitParameter("mockRavenAuth"));
    boolean testingMode = mockRavenAuth || mockPotteryBackend || mockDatabase;
    binder.bindConstant().annotatedWith(Names.named("testingMode")).to(testingMode);
  }

  @Provides
  @Singleton
  KeyLockManager createKeyLockManager() {
    return KeyLockManagers.newLock();
  }

  @Provides
  PotteryBackendImpl createPotteryBackendImpl() {
    return new PotteryBackendImpl(
        context.getInitParameter("potteryBackendUrl"),
        Integer.parseInt(context.getInitParameter("potteryBackendConnections")));
  }

  @PostConstruct
  public void startSshServer() {
    Injector injector = GuiceResteasyBootstrapServletContextListenerV3.getInjector();
    injector.getInstance(SshDaemon.class).start();
  }

  @PreDestroy
  public void stopSshServer() {
    Injector injector = GuiceResteasyBootstrapServletContextListenerV3.getInjector();
    injector.getInstance(SshDaemon.class).stop();
  }

  @PostConstruct
  public void startSubmissionPoller() {
    Injector injector = GuiceResteasyBootstrapServletContextListenerV3.getInjector();
    injector.getInstance(PollSubmissionWorker.class).start();
  }

  @PreDestroy
  public void stopSubmissionPoller() {
    Injector injector = GuiceResteasyBootstrapServletContextListenerV3.getInjector();
    injector.getInstance(PollSubmissionWorker.class).stop();
  }

  @PreDestroy
  public void stopDatabase() {
    Injector injector = GuiceResteasyBootstrapServletContextListenerV3.getInjector();
    injector.getInstance(Database.class).stop();
  }
}
