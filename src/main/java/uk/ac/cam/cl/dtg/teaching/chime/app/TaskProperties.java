package uk.ac.cam.cl.dtg.teaching.chime.app;

public class TaskProperties {

  public static final String CRSID_REPLACEMENT_STRING_KEY = "crsid-replacement-string";
  public static final String DEFAULT_CRSID_REPLACEMENT_STRING = "your-crsid";

  public static final String TASK_KIND_KEY = "task-kind";
  public static final String DEFAULT_TASK_KIND = "StatusInOutput";

  public static final String COURSE_KEY = "course";

  public static final String PROBLEM_STATEMENT_KEY_PREFIX = "problemStatement-";

  public static final String ACTIONS_KEY = "actions";

  public static final String AUTHOR_KEY = "author";

  public static final String ADDITIONAL_FILE_FILTER_PREFIX = "additionalFiles-";
}
