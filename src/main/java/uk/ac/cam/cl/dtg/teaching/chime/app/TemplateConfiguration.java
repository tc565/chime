package uk.ac.cam.cl.dtg.teaching.chime.app;

import static freemarker.template.Configuration.VERSION_2_3_26;

import com.google.inject.Inject;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import java.io.File;
import java.io.IOException;
import javax.servlet.ServletContext;

public class TemplateConfiguration {

  private final Configuration configuration;

  @Inject
  public TemplateConfiguration(ServletContext servletContext) {
    this.configuration = makeTemplateConfiguration(servletContext);
  }

  public TemplateConfiguration() {
    this.configuration = makeTemplateConfigurationForTesting();
  }

  Configuration getConfiguration() {
    return configuration;
  }

  private static Configuration makeTemplateConfiguration(ServletContext servletContext) {
    Configuration configuration = new Configuration(VERSION_2_3_26);
    configuration.setServletContextForTemplateLoading(servletContext, "WEB-INF/templates");
    configuration.setDefaultEncoding("UTF-8");
    configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    return configuration;
  }

  private static Configuration makeTemplateConfigurationForTesting() {
    Configuration configuration = new Configuration(VERSION_2_3_26);
    try {
      configuration.setDirectoryForTemplateLoading(new File("src/main/webapp/WEB-INF/templates"));
    } catch (IOException e) {
      throw new IllegalArgumentException("Failed to set template directory");
    }
    configuration.setDefaultEncoding("UTF-8");
    configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
    return configuration;
  }
}
